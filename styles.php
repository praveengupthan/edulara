<link rel="icon" type="image/png" sizes="32x32" href="img/favx32x32.png">
<!-- styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/slick.css">
  <link rel="stylesheet" href="css/easy-responsive-tabs.css">
  <link rel="stylesheet" href="css/icomoon.css">
  <link rel="stylesheet" href="css/grid-gallery.css">
  <!-- date picker -->
  <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />  
  <link rel="stylesheet" href="css/fileinput.css">
  <link rel="stylesheet" href="css/custom-forms.css">
  <!-- <link rel="stylesheet" href="css/jquery-steps.css"> -->
  <link rel="stylesheet" href="css/steps.css">

  <!-- webslide menu -->
  <link rel="stylesheet" href="css/fade-down.css">
  <link rel="stylesheet" href="css/webslidemenu.css">



    
   
  <!--/ styles -->
