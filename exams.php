<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header.php' ?>
  <!-- main -->
  <main class="subpage">
      <!-- apge header -->
      <div class="page-header course-header" id="s0">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Exams</li>
                        </ol>
                    </nav>
                    <h1 class="pt-3">Bank PO</h1>
                    <p>Get all the information like Recruitment Notification, Exam Pattern, Eligibility related to several Bank PO Exams here. Testbook offers memory based papers for Bank PO/MT Cadre Exams based on Latest Pattern & Syllabus both in Hindi & English. Also, get courses like IBPS PO Prelims Crash Course, Quant Cracker and much more.</p>
                    <p>
                        <a href="javascript:void(0)" class="pinkbtnlg">Signup for Free Test</a>
                    </p>
                    <!-- row -->
                    <div class="row py-4">
                     <!--/ col -->
                        <div class="col-lg-4 col-4">
                            <div class="">
                                <article>
                                    <h5>294</h5>
                                    <p>Total Tests</p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                        <!--/ col -->
                        <div class="col-lg-4 col-4">
                            <div>                               
                                <article>
                                    <h5>1200</h5>
                                    <p>Total Questions</p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                        <!--/ col -->
                        <div class="col-lg-4 col-4">
                            <div>                              
                                <article>
                                    <h5>2500</h5>
                                    <p>Tests Taken</p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
      </div>
      <!--/ page header -->

      <!-- pge body -->
      <div class="page-body">
            <!--container -->
            <div class="container">
                <!-- nav -->
                <nav class="scrollnav">
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="#s0">Courses</a></li>
                        <li class="nav-item"><a class="nav-link" href="#s1">Free Tests</a></li>
                        <li class="nav-item"><a class="nav-link" href="#s2">Test Series</a></li>
                        <li class="nav-item"><a class="nav-link" href="#s3">Quizzes</a></li>
                        <li class="nav-item"><a class="nav-link" href="#s4">Faq's</a></li>
                    </ul>
                </nav>
                <!-- /nav -->
                <!-- courses -->
                <div id="s0" class="section-col other-getdone">
                    <h3 class="h5 fblue fsbold text-uppercase pb-3">Courses (65) <span class="d-inline-block px-3">|</span><small><a href="javascript:void(0)" class="fgray d-inline-block">View All</a></small></h3>
                    <!-- courses -->
                     <!-- card row -->
                  <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course04.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course05.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course08.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>                
                <!--/ courses -->

                <!-- tests -->
                <div id="s1" class="section-col">
                    <h3 class="h5 fblue fsbold text-uppercase pb-4 pb-sm-2">Free Tests (13) <span class="d-inline-block px-3">|</span><small><a href="javascript:void(0)" class="fgray d-inline-block">View All</a></small></h3>

                    <!-- slider -->
                    <div class="custom-slick freetests-subpage freetests">
                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <!-- card -->
                            <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">
                                <p class="text-right">
                                    <span class="bluebtnlg text-uppercase">free</span>
                                </p>
                                <h5 class="h6 fbold">
                                    <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                                </h5>
                                <p class="fgray pb-4">IDBI Asst Manatger</p>

                                <p class="fgray">Expires on: June 30, 2020</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Questions</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Marks</span>
                                    <span class="fpink">200</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Minutes</span>
                                    <span class="fpink">120</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p>
                                <a href="javascript:void(0)" class="brdlink">Start Now</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ slide -->
                    </div>
                    <!--/ slider -->
                </div>
                <!--/ tests -->

                <!-- test series -->
                <div id="s2" class="section-col">

                <h3 class="h5 fblue fsbold text-uppercase">Test Series <span class="d-inline-block px-3">|</span><small><a href="javascript:void(0)" class="fgray d-inline-block">View All</a></small></h3>

                <!-- row -->
                <div class="row pt-3">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-6 pb-sm-3">
                        <!-- card -->
                        <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">                               
                                <h5 class="h6">
                                    <a href="javascript:void(0)" class="fgray">IBPS PO 2019 Mock Test</a>
                                </h5>                              

                                <p class="fgray small">50 Total Tests   <span class="px-2 d-inline-block">|</span>  6 free Tests</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Advanced Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Mains</span>
                                    <span class="fpink">20</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p class="pb-0">
                                <a href="javascript:void(0)" class="brdlink d-block text-center">View Test Series</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6 pb-sm-3">
                        <!-- card -->
                        <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">                               
                                <h5 class="h6">
                                    <a href="javascript:void(0)" class="fgray">IBPS PO 2019 Mock Test</a>
                                </h5>                              

                                <p class="fgray small">50 Total Tests   <span class="px-2 d-inline-block">|</span>  6 free Tests</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Advanced Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Mains</span>
                                    <span class="fpink">20</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p class="pb-0">
                                <a href="javascript:void(0)" class="brdlink d-block text-center">View Test Series</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6 pb-sm-3">
                        <!-- card -->
                        <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">                               
                                <h5 class="h6">
                                    <a href="javascript:void(0)" class="fgray">IBPS PO 2019 Mock Test</a>
                                </h5>                              

                                <p class="fgray small">50 Total Tests   <span class="px-2 d-inline-block">|</span>  6 free Tests</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Advanced Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Mains</span>
                                    <span class="fpink">20</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p class="pb-0">
                                <a href="javascript:void(0)" class="brdlink d-block text-center">View Test Series</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6 pb-sm-3">
                        <!-- card -->
                        <div class="card test-item">
                            <!-- card body -->
                            <div class="card-body">                               
                                <h5 class="h6">
                                    <a href="javascript:void(0)" class="fgray">IBPS PO 2019 Mock Test</a>
                                </h5>                              

                                <p class="fgray small">50 Total Tests   <span class="px-2 d-inline-block">|</span>  6 free Tests</p>
                                <p class="fblue small pb-3">Syllabus</p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Advanced Test - Prelims</span>
                                    <span class="fpink">20</span>
                                </p>
                                <p class="d-flex justify-content-between rowp">
                                    <span class="fgray">Full Test - Mains</span>
                                    <span class="fpink">20</span>
                                </p>
                            </div>
                            <!--/ card body -->
                            <!-- card footer -->
                            <div class="card-footer text-center">
                                <p class="pb-0">
                                <a href="javascript:void(0)" class="brdlink d-block text-center">View Test Series</a>
                                </p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
                </div>
                <!-- /test series -->

                  <!-- Quizzes -->
                  <div id="s3" class="section-col">

                    <h3 class="h5 fblue fsbold text-uppercase">Quizzes <span class="d-inline-block px-3">|</span><small><a href="javascript:void(0)" class="fgray d-inline-block">View All</a></small></h3>

                    <!-- row -->
                    <div class="row pt-3">
                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 pb-sm-3">
                            <!-- card -->
                            <div class="card test-item">
                                <!-- card body -->
                                <div class="card-body">   
                                    <p class="fgray small">Mixed Quiz </p>                            
                                    <h5 class="h6">
                                        <a href="javascript:void(0)" class="fgray">SBI PO - Mix Quiz</a>
                                    </h5>  
                                    <p class="fblue small pb-3">SBI PO</p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Questions</span>
                                        <span class="fpink">10</span>
                                    </p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Total Time</span>
                                        <span class="fpink">10 Min</span>
                                    </p>
                                </div>
                                <!--/ card body -->
                                <!-- card footer -->
                                <div class="card-footer text-center">
                                    <p class="pb-0">
                                    <a href="javascript:void(0)" class="brdlink d-block text-center">Start Quiz</a>
                                    </p>
                                </div>
                                <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-sm-6 pb-sm-3">
                            <!-- card -->
                            <div class="card test-item">
                                <!-- card body -->
                                <div class="card-body">   
                                    <p class="fgray small">Mixed Quiz </p>                            
                                    <h5 class="h6">
                                        <a href="javascript:void(0)" class="fgray">SBI PO - Mix Quiz</a>
                                    </h5>  
                                    <p class="fblue small pb-3">SBI PO</p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Questions</span>
                                        <span class="fpink">10</span>
                                    </p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Total Time</span>
                                        <span class="fpink">10 Min</span>
                                    </p>
                                </div>
                                <!--/ card body -->
                                <!-- card footer -->
                                <div class="card-footer text-center">
                                    <p class="pb-0">
                                    <a href="javascript:void(0)" class="brdlink d-block text-center">Start Quiz</a>
                                    </p>
                                </div>
                                <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 pb-sm-3">
                            <!-- card -->
                            <div class="card test-item">
                                <!-- card body -->
                                <div class="card-body">   
                                    <p class="fgray small">Mixed Quiz </p>                            
                                    <h5 class="h6">
                                        <a href="javascript:void(0)" class="fgray">SBI PO - Mix Quiz</a>
                                    </h5>  
                                    <p class="fblue small pb-3">SBI PO</p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Questions</span>
                                        <span class="fpink">10</span>
                                    </p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Total Time</span>
                                        <span class="fpink">10 Min</span>
                                    </p>
                                </div>
                                <!--/ card body -->
                                <!-- card footer -->
                                <div class="card-footer text-center">
                                    <p class="pb-0">
                                    <a href="javascript:void(0)" class="brdlink d-block text-center">Start Quiz</a>
                                    </p>
                                </div>
                                <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 pb-sm-3">
                            <!-- card -->
                            <div class="card test-item">
                                <!-- card body -->
                                <div class="card-body">   
                                    <p class="fgray small">Mixed Quiz </p>                            
                                    <h5 class="h6">
                                        <a href="javascript:void(0)" class="fgray">SBI PO - Mix Quiz</a>
                                    </h5>  
                                    <p class="fblue small pb-3">SBI PO</p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Questions</span>
                                        <span class="fpink">10</span>
                                    </p>
                                    <p class="d-flex justify-content-between rowp">
                                        <span class="fgray">Total Time</span>
                                        <span class="fpink">10 Min</span>
                                    </p>
                                </div>
                                <!--/ card body -->
                                <!-- card footer -->
                                <div class="card-footer text-center">
                                    <p class="pb-0">
                                    <a href="javascript:void(0)" class="brdlink d-block text-center">Start Quiz</a>
                                    </p>
                                </div>
                                <!--/ card footer -->
                            </div>
                            <!--/ card -->
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->
                    </div>
                    <!-- /Quizzes -->

                    <!-- faq's -->
                    <div id="s4" class="section-col">
                        <h3 class="h5 fblue fsbold text-uppercase">Faq's</h3>

                        <!-- tab quiz -->
                        <!--Vertical Tab-->
                        <div class="resp-vtabs">
                            <div class="parentVerticalTab ">
                                <ul class="resp-tabs-list hor_1">
                                    <li>About Bank PO Examinations for all years</li>
                                    <li>Know Every thing  About Testbook Pass</li>
                                    <li>FAQ's regarding Mock Tests</li>
                                </ul>
                                <div class="resp-tabs-container hor_1">
                                    <!-- faq 1-->
                                    <div> 
                                        <!-- custom accord -->
                                        <div class="accordion cust-accord">
                                            <h3 class="panel-title">What is Airtasker Pay?</h3>
                                            <div class="panel-content">
                                                <p>Airtasker Pay is our payment system that ensures payment protection for Taskers and a seamless, secure way to get local tasks completed for Posters.</p>
                                                <p>With Airtasker Pay, both Posters and Taskers can be confident that the money has been committed to the task, and that it will only be released when both sides are happy that the task is complete. </p>
                                                <p>The steps below outline the Airtasker payment flow under this system:</p>
                                            </div>

                                            <h3 class="panel-title">How can I increase the price once the task is assigned?</h3>
                                            <div class="panel-content">
                                                <p>Realised there’s more to be done or the task is more complicated than initially thought? Not to worry, increasing the task price is simple.</p>
                                                <p>Once an offer has been accepted, either the Poster or Tasker can increase the task price, using the steps below.</p>
                                                <p>The price can only be increased 3 times(to an overall maximum of 500 in your local currency) and the service fee is applied to all price increases.</p>
                                                <p>This feature is currently only available to increase the price. Should you need to decrease the task price, first have a chat with the other party through private messaging. Once you’ve agreed on a new price, contact us to assist. For more info on decreasing the price, see here.</p>
                                            </div>

                                            <h3 class="panel-title">How do I decrease the price?</h3>
                                            <div class="panel-content">
                                                <p>Decreasing the price can only be done if the task funds are still held in escrow. If the payment has already been released, Airtasker no longer has access to it, so Posters and Taskers will need to organise adjustments directly.</p>
                                                <p>If payment hasn't yet been released yet and you would like to decrease the task price, you can do so by following these steps:</p>
                                                <ol>
                                                    <li class="p1">Discuss with the other party about decreasing the task price and come to an agreement.</li>
                                                    <li class="p1"><span style="font-weight: 400;">Both parties must agree to decrease the price via Airtasker private messages and explicitly state the new amount.</span></li>
                                                    <li class="p1">Contact Airtasker (<a href="https://support.airtasker.com/hc/en-au/articles/213536878-How-can-I-contact-Airtasker-">click here</a>) once both parties have written that they agree to a decrease in the task price and stated what the new amount should be.</li>
                                                    <li class="p1">Airtasker will adjust the price on the system accordingly and <a href="/hc/en-au/articles/221269248" target="_self">return</a> the extra amount to the Poster.</li>
                                                </ol>
                                                <p>The price can only be increased 3 times(to an overall maximum of 500 in your local currency) and the service fee is applied to all price increases.</p>
                                                <p>This feature is currently only available to increase the price. Should you need to decrease the task price, first have a chat with the other party through private messaging. Once you’ve agreed on a new price, contact us to assist. For more info on decreasing the price, see here.</p>
                                                <p>Please note that we won't be able to help adjust the task price until both parties have confirmed that they're happy to lower the task price and explicitly stated the new amount. The agreed amount should be inclusive of Airtasker fees to avoid confusion and must be rounded to the nearest full number.</p>
                                            </div>

                                            <h3 class="panel-title">I don't agree with an increase price request, what do I do?</h3>
                                            <div class="panel-content">
                                                <p>Both members have the option to increase the task price after being assigned. When a Tasker uses this feature, a request will be sent to you (the Poster) with details about the request.</p>
                                                <p>If you don’t agree with the request to increase the task price, you can either decline it, or discuss the request with the Tasker. The request will not be accepted without your express permission.</p>
                                                
                                                <p>If you’re not sure about a Tasker’s request for an increase in payment, first send them through a private message to ask for more information. Taskers are generally friendly and open to negotiation so it’s definitely worth chatting with them to come to an agreement.</p>
                                                <p>You can also choose to decline the request. This can be done in the following steps:</p>
                                                <p>If you can't come to an agreement about the final price of a task, Airtasker can help. Check out our range of Dispute Resolution articles for more information here.</p>
                                            </div>
                                        </div>
                                        <!--/ custom accord -->
                                    </div>
                                    <!-- faq 1-->

                                    <!-- faq 2 -->
                                    <div> 
                                          <!-- custom accord -->
                                          <div class="accordion cust-accord">
                                             <h3 class="panel-title">How do I check my payment history?</h3>
                                            <div class="panel-content">
                                                <p>Your payment history will have information about all incoming and outgoing payments through Airtasker. You can easily check this through the following steps:</p>
                                                <ol>
                                                    <li class="p1">Log into Airtasker.</li>
                                                    <li class="p1">Click the <em>Payment History&nbsp;</em>tab in your account. You can find this under the <em>More</em>&nbsp;<em>Options</em> menu in the app, or by clicking your profile picture in the mobile or web browser</li>
                                                    <li class="p1">Select on <em>Earned</em> or <em>Outgoing</em> depending on which payment history you'd like to view</li>
                                                </ol>
                                                
                                                <p>You'll then be able to see a list of all the relevant transactions, including the date, task details and a breakdown of the price.</p>
                                                <p>You can also choose to decline the request. This can be done in the following steps:</p>
                                                <p>You can also download a CSV file of all of your payments by clicking on Download (which can be found below the net earned or net outgoing amount).</p>
                                            </div>

                                        <h3 class="panel-title">What are bonuses?</h3>
                                        <div class="panel-content">
                                            <p>When a Tasker does a great job on a task, you can give them a little bit extra to say thanks!</p>
                                            
                                            <p>We've created the bonus feature on our platform to help you easily do this. You will see this option when you’re releasing the payment to the Tasker.</p>
                                            <p>You’ll be shown the bonus options available when you click Release Payment. At the moment, bonuses can only be added in pre-set amounts, and you can't increase the total task value to over our maximum of 9,999 (in your local currency).</p>
                                            <p>If you don’t want to add a bonus, that’s okay too! On the desktop site, just stay on the pre-selected option of I’d prefer not to and proceed to release payment. On the app, you can simply release payment without selecting any of the bonus amounts. </p>
                                            <p>Keep in mind that all payments through Airtasker have a service fee deducted, including bonuses. </p>
                                        </div>

                                        <h3 class="panel-title">How do coupons work on Airtasker?</h3>
                                        <div class="panel-content">
                                            <p>Coupons are unique codes that give you a discount on tasks posted on Airtasker. Coupons can be distributed in a number of ways, including as part of promotions, or as prizes for competitions on our Facebook page.</p>
                                            <p>Coupons have specific requirements that need to be met so you can redeem their value. Check for the following:</p>
                                            <p>If the requirements are met and your coupon is valid, you can use it for a discount on a task. You can pay any extra amount on the task with the saved payment method on your account.</p>
                                            <p>Please keep in mind that you can only use one coupon per task.</p>
                                            <p>Coupons are redeemed when you accept an offer on a task. On the payment screen before you assign a Tasker, there will be a button that says Add a coupon under your payment method. Enter your code here before clicking assign. Check out this process in action below:</p>
                                        </div>

                                        <h3 class="panel-title">What are referrals? How do they work?</h3>
                                        <div class="panel-content">
                                            <p>When you become a member of Airtasker you get your own unique referral code to share with friends and family. If someone signs up using your code, they’ll get $25 towards their first task. And once their task is completed, you’ll get $10 credit yourself!</p>
                                            <p>The easiest way to share the code is on your social media accounts. That way you have the best chance of spreading the Airtasker love (and credit) to people you know.</p>
                                            <p>Although the code doesn't expire, it can only be used by a maximum of 15 people, so do keep that in mind. If you're having trouble with any referral matters, check out our helpful article here.</p>
                                            <p>If you've signed up using someone's referral code, you'll get the $25 added to your account to use on your first task (to the value of $100 or more). This will be automatically added to your account and automatically applied when you assign your first task.</p>
                                            <p>For any issues with referrals, see our helpful article here.</p>
                                        </div>

                                        <h3 class="panel-title">The task is complete, can I still increase the task price?</h3>
                                        <div class="panel-content">
                                            <p>If the Tasker has requested payment already but you need to increase the price, you can still do so.</p>
                                            <p>Just so you know, once payment has been released you're no longer able to change the price. </p>
                                            <p>You can also choose to increase the payment using the bonus feature, which you can find out more about here.</p>
                                        </div>
                                          </div>
                                          <!-- custom accord -->

                                    </div>
                                    <!-- faq 2-->

                                    <!-- faq 3-->
                                    <div> 
                                         <!-- custom accord -->
                                         <div class="accordion cust-accord">
                                         <h3 class="panel-title">What are bonuses?</h3>
                                        <div class="panel-content">
                                            <p>When a Tasker does a great job on a task, you can give them a little bit extra to say thanks!</p>
                                            
                                            <p>We've created the bonus feature on our platform to help you easily do this. You will see this option when you’re releasing the payment to the Tasker.</p>
                                            <p>You’ll be shown the bonus options available when you click Release Payment. At the moment, bonuses can only be added in pre-set amounts, and you can't increase the total task value to over our maximum of 9,999 (in your local currency).</p>
                                            <p>If you don’t want to add a bonus, that’s okay too! On the desktop site, just stay on the pre-selected option of I’d prefer not to and proceed to release payment. On the app, you can simply release payment without selecting any of the bonus amounts. </p>
                                            <p>Keep in mind that all payments through Airtasker have a service fee deducted, including bonuses. </p>
                                        </div>

                                        <h3 class="panel-title">How do coupons work on Airtasker?</h3>
                                        <div class="panel-content">
                                            <p>Coupons are unique codes that give you a discount on tasks posted on Airtasker. Coupons can be distributed in a number of ways, including as part of promotions, or as prizes for competitions on our Facebook page.</p>
                                            <p>Coupons have specific requirements that need to be met so you can redeem their value. Check for the following:</p>
                                            <p>If the requirements are met and your coupon is valid, you can use it for a discount on a task. You can pay any extra amount on the task with the saved payment method on your account.</p>
                                            <p>Please keep in mind that you can only use one coupon per task.</p>
                                            <p>Coupons are redeemed when you accept an offer on a task. On the payment screen before you assign a Tasker, there will be a button that says Add a coupon under your payment method. Enter your code here before clicking assign. Check out this process in action below:</p>
                                        </div>

                                        <h3 class="panel-title">What are referrals? How do they work?</h3>
                                        <div class="panel-content">
                                            <p>When you become a member of Airtasker you get your own unique referral code to share with friends and family. If someone signs up using your code, they’ll get $25 towards their first task. And once their task is completed, you’ll get $10 credit yourself!</p>
                                            <p>The easiest way to share the code is on your social media accounts. That way you have the best chance of spreading the Airtasker love (and credit) to people you know.</p>
                                            <p>Although the code doesn't expire, it can only be used by a maximum of 15 people, so do keep that in mind. If you're having trouble with any referral matters, check out our helpful article here.</p>
                                            <p>If you've signed up using someone's referral code, you'll get the $25 added to your account to use on your first task (to the value of $100 or more). This will be automatically added to your account and automatically applied when you assign your first task.</p>
                                            <p>For any issues with referrals, see our helpful article here.</p>
                                        </div>

                                        <h3 class="panel-title">The task is complete, can I still increase the task price?</h3>
                                        <div class="panel-content">
                                            <p>If the Tasker has requested payment already but you need to increase the price, you can still do so.</p>
                                            <p>Just so you know, once payment has been released you're no longer able to change the price. </p>
                                            <p>You can also choose to increase the payment using the bonus feature, which you can find out more about here.</p>
                                        </div>
                                         </div>
                                         <!-- /custom accord -->

                                    </div>
                                    <!--/ faq 3-->
                                </div>
                            </div>
                            </div>
                        <!--/ tab quiz -->
                    </div>                                            
                    <!--/ faq's -->
            </div>
            <!--/ container -->
      </div>
      <!--/ page bodyt -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>

  <script>
    //scroller
$(document).ready(function () {
    $('.scrollnav').scroller();
});
  </script>
  

</body>

</html>