  <!-- header -->
  <script type='text/javascript'>
    $(document).ready(function () {
      $("a[data-theme]").click(function () {
        $("head link#theme").attr("href", $(this).data("theme"));
        $(this).toggleClass('active').siblings().removeClass('active');
      });
      $("a[data-effect]").click(function () {
        $("head link#effect").attr("href", $(this).data("effect"));
        $(this).toggleClass('active').siblings().removeClass('active');
      });

    });
  </script>
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
  
  <header class="fixed-top">
    <!-- container -->
    <div class="container">    
    <!-- nav start-->

  <!-- Mobile Header -->
  <div class="wsmobileheader clearfix">
    <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
    <span class="smllogo"><span class="icon-logo"></span></span>
    
  </div>
  <!-- Mobile Header -->

  <div class="headerfull">
    <div class="wsmain clearfix">
      <div class="smllogo navbar-brand"><a href="#"><span class="icon-logo"></span></a></div>
      <nav class="wsmenu clearfix">
        <ul class="wsmenu-list">
          <li aria-haspopup="true"><a href="#" class="navtext"><span><span class="icon-interface icomoon"></span> Categories </span></a>
            <div class="wsshoptabing wtsdepartmentmenu clearfix">
              <div class="wsshopwp clearfix">
                <ul class="wstabitem clearfix">
                  <li class="wsshoplink-active"><a href="#"> Banking & Finance </a>
                    <div class="wstitemright clearfix wstitemrightactive">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 clearfix">
                            <div class="wstheading clearfix">Banking & Finance</div>
                            <ul class="wstliststy01 clearfix">
                              <li><a href="javascript:void(0)">All Accounts & Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Accounting Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Finance Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Financial Statement</a></li>
                              <li><a href="javascript:void(0)">SAP FICCO</a></li>
                              <li><a href="javascript:void(0)">Xero</a></li>
                              <li><a href="javascript:void(0)">Cost Accounting</a></li>
                              <li><a href="javascript:void(0)">Accounts Advance</a></li>
                              <li><a href="javascript:void(0)">All Accounts & Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Accounting Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Finance Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Financial Statement</a></li>
                              <li><a href="javascript:void(0)">SAP FICCO</a></li>
                              <li><a href="javascript:void(0)">Xero</a></li>
                              <li><a href="javascript:void(0)">Cost Accounting</a></li>
                              <li><a href="javascript:void(0)">Accounts Advance</a></li>
                              <li><a href="javascript:void(0)">All Accounts & Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Accounting Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Finance Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Financial Statement</a></li>
                              <li><a href="javascript:void(0)">SAP FICCO</a></li>
                              <li><a href="javascript:void(0)">Xero</a></li>
                              <li><a href="javascript:void(0)">Cost Accounting</a></li>
                              <li><a href="javascript:void(0)">Accounts Advance</a></li>
                            </ul> 
                          </div>                         
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#"> Accounts</a>
                    <div class="wstitemright clearfix">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 clearfix">
                            <div class="wstheading clearfix">Account</div>
                            <ul class="wstliststy01 clearfix">
                              <li><a href="javascript:void(0)">All Accounts & Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Accounting Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Finance Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Financial Statement</a></li>
                              <li><a href="javascript:void(0)">SAP FICCO</a></li>
                              <li><a href="javascript:void(0)">Xero</a></li>
                              <li><a href="javascript:void(0)">Cost Accounting</a></li>
                              <li><a href="javascript:void(0)">Accounts Advance</a></li>
                              <li><a href="javascript:void(0)">All Accounts & Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Accounting Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Finance Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Financial Statement</a></li>
                              <li><a href="javascript:void(0)">SAP FICCO</a></li>
                              <li><a href="javascript:void(0)">Xero</a></li>
                              <li><a href="javascript:void(0)">Cost Accounting</a></li>
                              <li><a href="javascript:void(0)">Accounts Advance</a></li>
                              <li><a href="javascript:void(0)">All Accounts & Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Accounting Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Finance Fundamentals</a></li>
                              <li><a href="javascript:void(0)">Book Keeping</a></li>
                              <li><a href="javascript:void(0)">Financial Statement</a></li>
                              <li><a href="javascript:void(0)">SAP FICCO</a></li>
                              <li><a href="javascript:void(0)">Xero</a></li>
                              <li><a href="javascript:void(0)">Cost Accounting</a></li>
                              <li><a href="javascript:void(0)">Accounts Advance</a></li>
                            </ul> 
                          </div>
                         
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#"> IT &amp; Software </a>
                    <div class="wstitemright clearfix">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-3 col-md-12 clearfix">
                            <ul class="wstliststy02 clearfix">
                              <li class="wstheading clearfix">Latest Movies</li>
                              <li><a href="#">Action & Adventure </a></li>
                              <li><a href="#">Bollywood </a></li>
                              <li><a href="#">Documentary</a></li>
                              <li><a href="#">Educational</a></li>
                              <li><a href="#">Exercise & Fitness </a></li>
                              <li><a href="#">Faith & Spirituality</a></li>
                              <li><a href="#">Fantasy</a></li>
                              <li><a href="#">Romance</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12 clearfix">
                            <ul class="wstliststy02 clearfix">
                              <li class="wstheading clearfix">Newest Games</li>
                              <li><a href="#">PlayStation 4 </a> </li>
                              <li><a href="#">Xbox 1 </a> </li>
                              <li><a href="#">Nintendo DS</a> </li>
                              <li><a href="#">PlayStation Vita </a> </li>
                              <li><a href="#">Retro Gaming</a> </li>
                              <li><a href="#">Digital Games</a> </li>
                              <li><a href="#">Microconsoles</a> </li>
                              <li><a href="#">Kids & Family </a> </li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12 clearfix">
                            <ul class="wstliststy02 clearfix">
                              <li class="wstheading clearfix">Popular Music Genre</li>
                              <li><a href="#">Alternative & Indie Rock</a> </li>
                              <li><a href="#">Broadway & Vocalists</a> </li>
                              <li><a href="#">Children's Music</a> </li>
                              <li><a href="#">Christian </a> </li>
                              <li><a href="#">Classic Rock</a> </li>
                              <li><a href="#">Comedy & Miscellaneous </a> </li>
                              <li><a href="#">Country</a> </li>
                              <li><a href="#">Dance & Electronic</a> </li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12 clearfix">
                            <ul class="wstliststy02 clearfix">
                              <li class="wstheading clearfix">Popular Music Genre</li>
                              <li><a href="#">Alternative & Indie Rock</a> </li>
                              <li><a href="#">Broadway & Vocalists</a> </li>
                              <li><a href="#">Children's Music </a></li>
                              <li><a href="#">Classical</a> </li>
                              <li><a href="#">Classic Rock</a> </li>
                              <li><a href="#">Comedy & Miscellaneous</a> </li>
                              <li><a href="#">Country</a> </li>
                              <li><a href="#">Dance & Electronic</a> </li>
                            </ul>
                          </div>                        
                        
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#">Engineering </a>
                    <div class="wstitemright clearfix kitchenmenuimg">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy02 clearfix">
                              <li class="wstheading clearfix">Engineering</li>
                              <li><a href="#">Air Conditioners </a></li>
                              <li><a href="#">Air Coolers </a></li>
                              <li><a href="#">Fans</a></li>
                              <li><a href="#">Microwaves</a></li>
                              <li><a href="#">Refrigerators</a></li>
                              <li><a href="#">Washing Machines </a></li>
                              <li><a href="#">Jars & Containers </a></li>
                              <li><a href="#">LED & CFL bulbs </a></li>
                              <li><a href="#">Drying Racks </a></li>
                              <li><a href="#">Laundry Baskets</a> </li>
                              <li><a href="#">Washing Machines </a></li>
                              <li><a href="#">Bedsheets </a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy02 clearfix">
                              <li class="wstheading clearfix">Engineering</li>
                              <li><a href="#">Air Fryers </a></li>
                              <li><a href="#">Espresso Machines</a></li>
                              <li><a href="#">Food Processors</a> </li>
                              <li><a href="#">Hand Blenders</a></li>
                              <li><a href="#">Induction Cooktops</a></li>
                              <li><a href="#">Microwave Ovens</a></li>
                              <li><a href="#">Mixers & Grinders</a></li>
                              <li><a href="#">Rice Cookers</a></li>
                              <li><a href="#">Stand Mixers</a></li>
                              <li><a href="#">Sandwich Makers</a></li>
                              <li><a href="#">Tandoor & Grills</a></li>
                              <li><a href="#">Toasters</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#">Education &amp; Teaching</a>
                    <div class="wstitemright clearfix">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy02 clearfix">                             
                              <li class="wstheading clearfix">TV & Audio</li>
                              <li><a href="#">4K Ultra HD TVs </a></li>
                              <li><a href="#">LED & LCD TVs</a></li>
                              <li><a href="#">OLED TVs</a> </li>
                              <li><a href="#">Plasma TVs</a></li>
                              <li><a href="#">Smart TVs</a></li>
                              <li><a href="#">Home Theater</a></li>
                              <li><a href="#">Wireless & streaming</a></li>
                              <li><a href="#">Stereo System</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy02 clearfix"> 
                              <li class="wstheading clearfix">Camera, Photo & Video</li>
                              <li><a href="#">Accessories <span class="wstcount">(1145)</span></a></li>
                              <li><a href="#">Bags & Cases <span class="wstcount">(445)</span></a></li>
                              <li><a href="#">Binoculars & Scopes <span class="wstcount">(45)</span></a></li>
                              <li><a href="#">Digital Cameras <span class="wstcount">(845)</span></a> </li>
                              <li><a href="#">Film Photography <span class="wstcount">(245)</span></a> </li>
                              <li><a href="#">Flashes <span class="wstcount">(105)</span></a></li>
                              <li><a href="#">Lenses <span class="wstcount">(445)</span></a></li>
                              <li><a href="#">Video <span class="wstcount">(145)</span> </a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy02 clearfix">                            
                              <li class="wstheading clearfix">Phones & Accessories</li>
                              <li><a href="#">Unlocked Cell Phones </a></li>
                              <li><a href="#">Smartwatches </a></li>
                              <li><a href="#">Carrier Phones</a></li>
                              <li><a href="#">Cell Phone Cases</a> </li>
                              <li><a href="#">Bluetooth Headsets</a></li>
                              <li><a href="#">Cell Phone Accessories</a></li>
                              <li><a href="#">Fashion Tech</a></li>
                              <li><a href="#">Headphone</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy02 clearfix">                             
                              <li class="wstheading clearfix">Wearable Device</li>
                              <li><a href="#">Activity Trackers </a></li>
                              <li><a href="#">Sports & GPS Watches</a></li>
                              <li><a href="#">Smart Watches</a> </li>
                              <li><a href="#">Virtual Reality Headsets</a></li>
                              <li><a href="#">Wearable Cameras</a></li>
                              <li><a href="#">Smart Glasses</a></li>
                              <li><a href="#">Kids & Pets</a></li>
                              <li><a href="#">Smart Sport Accessories</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#">Civil Exams</a>
                    <div class="wstitemright clearfix computermenubg">
                      <div class="wstmegamenucoll01 clearfix">
                        <div class="container-fluid">
                          <div class="row">
                            <div class="col-lg-8">
                              <div class="wstheading clearfix">Monitors <a href="#" class="wstmorebtn">View All</a></div>
                              <ul class="wstliststy03 clearfix">
                                <li><a href="#">50 Inches <span class="wstmenutag greentag">New</span></a></li>
                                <li><a href="#">40 to 49.9 Inches </a></li>
                                <li><a href="#">30 to 39.9 Inches</a></li>
                                <li><a href="#">26 to 29.9 Inches</a></li>
                                <li><a href="#">18 to 19.9 Inches</a></li>
                                <li><a href="#">16 to 17.9 Inches</a></li>
                                <li><a href="#">26 to 29.9 Inches</a></li>
                                <li><a href="#">18 to 19.9 Inches</a></li>
                                <li><a href="#">16 to 17.9 Inches</a></li>
                              </ul>
                            </div>
                            <div class="col-lg-8">
                              <div class="wstheading clearfix">Software <a href="#" class="wstmorebtn">View All</a></div>
                              <ul class="wstliststy03 clearfix">
                                <li><a href="#">Antivirus & Security</a> </li>
                                <li><a href="#">Business </a> </li>
                                <li><a href="#">Web Design</a> </li>
                                <li><a href="#">Digital Software</a> </li>
                                <li><a href="#">Education & Reference</a> </li>
                                <li><a href="#">Lifestyle & Hobbies</a> </li>
                                <li><a href="#">Digital Software</a> </li>
                                <li><a href="#">Education & Reference</a> </li>
                                <li><a href="#">Lifestyle & Hobbies</a> </li>
                              </ul>

                            </div>
                            <div class="col-lg-8">
                              <div class="wstheading clearfix">Accessories <a href="#" class="wstmorebtn">View All</a></div>
                              <ul class="wstliststy03 clearfix">
                                <li><a href="#">Audio & Video Accessories</a> </li>
                                <li><a href="#">Cable Security Devices</a> </li>
                                <li><a href="#">Input Devices </a> </li>
                                <li><a href="#">Memory Cards</a> </li>
                                <li><a href="#">Monitor Accessories</a> </li>
                                <li><a href="#">USB Gadgets</a> </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#">Photography</a>
                    <div class="wstitemright clearfix wstpngsml">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                              
                              <li class="wstheading clearfix"><a href="#">Interior</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                             
                              <li class="wstheading clearfix"><a href="#">Styling</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                              
                              <li class="wstheading clearfix"><a href="#">Utility</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                           
                              <li class="wstheading clearfix"><a href="#">Spare Parts</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                             
                              <li class="wstheading clearfix"><a href="#">Protection</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">                             
                              <li class="wstheading clearfix"><a href="#">Cleaning</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                             
                              <li class="wstheading clearfix"><a href="#">Car Audio</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-12">
                            <ul class="wstliststy04 clearfix">
                             
                              <li class="wstheading clearfix"><a href="#">Gear & Accessories</a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#">Other Technologies</a>
                    <div class="wstitemright clearfix wstpngsml">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-4 col-md-12 wstmegamenucolr03 clearfix"><a href="#"><img src="images/health-menu-img01.jpg"
                                alt=""></a></div>
                          <div class="col-lg-12 col-md-12 clearfix">
                            <div class="container-fluid">
                              <div class="row">
                                <div class="col-lg-4 col-md-12">
                                  <ul class="wstliststy05 clearfix">                                   
                                    <li class="wstheading clearfix">Health Care</li>
                                    <li><a href="#">Diabetes </a></li>
                                    <li><a href="#">Incontinence </a></li>
                                    <li><a href="#">Cough & Cold</a></li>
                                    <li><a href="#">Baby Care</a> </li>
                                    <li><a href="#">Women's Health</a></li>
                                    <li><a href="#">First Aid</a></li>
                                    <li><a href="#">Smoking Cessation</a></li>
                                    <li><a href="#">Sleep & Snoring</a></li>
                                  </ul>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                  <ul class="wstliststy05 clearfix">                                   
                                    <li class="wstheading clearfix">Personal Care</li>
                                    <li><a href="#">Hair Removal</a></li>
                                    <li><a href="#">Feminine Hygiene</a></li>
                                    <li><a href="#">Oral </a></li>
                                    <li><a href="#">Foot Care</a> </li>
                                    <li><a href="#">Hand Care</a></li>
                                    <li><a href="#">Personal Care Appliances</a></li>
                                    <li><a href="#">Foams & Creams</a></li>
                                    <li><a href="#">Hair Removal Creams</a></li>
                                  </ul>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                  <ul class="wstliststy05 clearfix">                                   
                                    <li class="wstheading clearfix">Medical Equipment</li>
                                    <li><a href="#">Tapes </a></li>
                                    <li><a href="#">Neck Supports</a></li>
                                    <li><a href="#">Arm Supports</a> </li>
                                    <li><a href="#">Elbow Braces</a></li>
                                    <li><a href="#">Knee & Leg Braces</a></li>
                                    <li><a href="#">Ankle Braces</a></li>
                                    <li><a href="#">Foot Supports</a></li>
                                    <li><a href="#">Crepe Bandages</a></li>

                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </li>  
        </ul>
        <ul class="wsmenu-list wsmenu-rt">
          <li aria-haspopup="true" class="wsshopmyaccount"><a href="#">Teach on Edulara</a></li>
          <li aria-haspopup="true" class="wsshopmyaccount"><a href="#">Teach on Edulara</a></li>
          <li aria-haspopup="true" class="wsshopmyaccount"><a data-toggle="modal" data-target="#signup" href="#">Signup</a></li>
          <li aria-haspopup="true" class="wsshopmyaccount"><a data-toggle="modal" data-target="#login" href="#">Login</a></li>
          <li aria-haspopup="true" class="wsshopmyaccount"><a class="cart" href="#"><span class="icon-commerce-and-shopping icomoon"></span><span class="value">12</span></a></li>
        </ul>
      </nav>
    </div>
  </div>
    <!--/ nav ends -->     
    </div>
    <!--/ container -->
  </header>
  <!--/ header -->

  <!-- modal popup for login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content  w-75">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-form">
          <!-- form -->
          <form>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <!-- form group -->
              <div class="form-group">
                  <label>Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <p class="text-right pb-2">
                <a href="javascript:void(0)" id="forgotpwlink" class="fblack">Forgot Password?</a>
              </p>
              <input type="submit" value="Login" class="w-100 pinkbtn">
          </form>
          <!--/ form -->

          <p class="text-center or py-2"><span>OR</span></p>
          <p class="text-center">Signin with your Social Networks</p>

          <div class="d-flex justify-content-between py-3">
              <a class="socialbtn fbbtn" href="javascript:void(0)">
                  <span class="icon-facebook icomoon"></span> 
                  Facebook
              </a>
              <a class="socialbtn googlesocial" href="javascript:void(0)">
                  <span class="icon-google-plus icomoon"></span> 
                  Google Plus
              </a>
          </div>
      </div>
      <div class="modal-footer d-flex justify-content-between">
          <span>Don't have an account ?</span>
          <a href="javascript:void(0)" id="signuplink" class="fpink">Sign up</a>
      </div>
    </div>
  </div>
</div>
  <!--/ modal popup for login -->

  <!-- modal popup for forgotpassword -->
<div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content  w-75">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Forgot Password?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-form">
          <!-- form -->
          <form>
            <p>Enter your email below and we will send you instructions on how to reset your password</p>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->             
             
              <input type="submit" value="Send" class="w-100 pinkbtn">
          </form>
          <!--/ form -->  
      </div>      
    </div>
  </div>
</div>
  <!--/ modal popup for forgot Password -->

   <!-- modal popup for signup -->
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content  w-75">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Join us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-form">
          <!-- form -->
          <form>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <!-- form group -->
              <div class="form-group">
                  <label>Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              
              <input type="submit" value="Join Laratasker" class="w-100 pinkbtn">
          </form>
          <!--/ form -->

          <p class="text-center or py-2"><span>OR</span></p>
          <p class="text-center">Signin with your Social Networks</p>

          <div class="d-flex justify-content-between py-3">
              <a class="socialbtn fbbtn" href="javascript:void(0)">
                  <span class="icon-facebook icomoon"></span> 
                  Facebook
              </a>
              <a class="socialbtn googlesocial" href="javascript:void(0)">
                  <span class="icon-google-plus icomoon"></span> 
                  Google Plus
              </a>
          </div>
          <p>
          <span><input type="checkbox"></span>  
          Please don't send me tips or marketing via email or sms.
        </p>
        <p class="py-2">By signing up, I agree to Airtasker's 
          <a href="javascript:void(0)" class="fpink">Terms &amp; Conditions, </a>
          <a href="javascript:void(0)" class="fpink">Community Guidelines, </a>
          <a href="javascript:void(0)" class="fpink">Privacy Policy</a>
        </p>
      </div>
      <div class="modal-footer d-flex justify-content-between">
          <span>Already have an account ?</span>
          <a href="javascript:void(0)" id="loginlink" class="fpink">Log in</a>
      </div>
    </div>
  </div>
</div>
  <!--/ modal popup for signup -->


  <!-- modal popup for Post task -->
<div class="modal fade post-task" id="posttask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content ">
      <!--- header -->
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">&nbsp;</h5>
        <button id="btn-confirm-exit-postatask" type="button" class="close"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--/ header -->

      <!-- body -->
      <div class="modal-body stepbody">
        <!-- div id wizard-->
        <div id="postTask">
            <!-- SECTION 1 -->
            <h4></h4>
            <section>
                <h5 class="text-center">Start getting offers in no time</h5>                    
                <div class="text-center">
                  <img class="my-4" src="img/onboard.png" alt="" width="150">
                  <p class="text-center">We're just going to ask a few questions to help you find the right Tasker - it'll only take a few minutes!</p>  
                </div>                                   
            </section>
            <!--/ SECTION 1 -->
              
            <!-- SECTION 2 -->
            <h4></h4>
            <section>
                <h5>Tell us what you need done?</h5>

                <div class="form-row mt-2">                  
                  <div class="col-12">     
                    <label>What do you need done?</label>                
                      <input class="multisteps-form__input form-control" type="text" placeholder="e.g. Help move my sofa"/>
                      <small> This'll be the title of your task -</small>
                  </div>
                </div>
                <div class="form-row mt-2">
                  <div class="col-12">
                    <label>What are the details?</label>       
                    <textarea class="form-control" type="text" placeholder="Be as specific as you can about what needs doing" style="height:100px;"></textarea>
                  </div>
                </div>
                
            </section>
            <!-- / SECTION 2-->

            <!-- SECTION 3 -->
            <h4></h4>
            <section>
                <h5>Say where & when</h5>

                <div class="row inpersonrow">
                  <div class="col-12 col-md-6 mt-2">
                    <div class="card shadow-sm">
                      <div class="card-body">
                        <label>
                            <input type="radio">
                              Inperson
                        </label>                         
                        <p class="card-text">Select this if you need the Tasker physically there.</p>
                        <p  class="pt-3"><span class="icon-location h3"></span></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 mt-2">
                    <div class="card shadow-sm">
                      <div class="card-body">
                      <label>
                            <input type="radio">
                              Online
                        </label>   
                        <p class="card-text">Select this if the Tasker can do it from home</p>
                        <p class="pt-3"><span class="icon-mobile h3"></span></p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-row mt-4">   

                  <div class="col-12">     
                    <label>Enter Suburb</label>                
                      <input class="multisteps-form__input form-control" type="text" placeholder="Enter Suburb"/> 
                  </div>
                  <div class="col-12 pt-3">     
                    <label>When do you need it done?</label>                
                      <input id="datepicker" class="form-control date" type="text" placeholder="Select Date"/> 
                  </div>
                </div>                     
            </section>
            <!--/ Section 3-->
			
			<script>
				  //datepicker
				$('#datepicker').datepicker({
					showOtherMonths: true
				});
			</script>

            <!-- SECTION 4 -->
            <h4></h4>
            <section>   
              <h5>Waht is your Budget</h5>     
              <p>Please enter the price you're comfortable to pay to get your task done. Taskers will use this as a guide for how much to offer.</p>
              <p class="py-2">
                <label><input type="radio">Total</label>
                <label><input type="radio">Hourly Rate</label>

                <div class="form-row mt-4">   
                  <div class="col-6">     
                    <label>Enter Value in Rupees</label>                
                      <div class="input-group d-flex">
                        <input class="multisteps-form__input form-control" type="text" placeholder="Rs:150"/> 
                        <span class="d-inline-block p-2"> X </span>
                        <input class="multisteps-form__input form-control" type="text" placeholder="2 Hours"/> 
                      </div>
                  </div>                   
              </div> 

              <div class="form-row mt-4">   
                  <div class="col-12">
                    <div class="bluebg p-3 d-flex justify-content-between">
                        <div>
                          <h5>ESTIMATED BUDGET</h5>
                          <p>Final payment will be agreed later</p>
                        </div>
                        <div class="align-self-center">
                            <h3 class="h3"><span class="icon-inr"></span> 250</h3>
                        </div>
                    </div>
                  </div>
              </div>
              </p>
              
          </section>
          <!-- Section 4-->

            <!-- SECTION 5 -->
            <h4></h4>
            <section>                    
                <div class="text-center">
                <img src="img/mail-success.png" alt="" width="150">
                  <h4 class="h4 py-3">Pick an offer</h4>
                  <p>Now Taskers can ask questions and make offers to do your task - make sure you check back on it regularly!</p> 
                </div>                  
            </section>
            <!-- Section 5-->
          </div>
          <!--/ div id wizard -->
     
      </div>
      <!--/ body -->          
    </div>
  </div>
</div>
  <!--/ modal popup for Post task -->



  <!-- modal popup for exit confirmation of post a task -->
<div class="modal fade" id="task-close-confirmbox" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content w-75 mx-auto">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Sorry to see you go...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">         
          <p >Are you sure? You're almost done and it's free to post a task...</p>        
      </div>
      <div class="modal-footer d-flex justify-content-between">
          <a href="javascript:void(0)" data-dismiss="modal"  id="signuplink" class="pinkbtnlg">Continue Task</a>
          <a href="javascript:void(0)" id="discardexitlink" class="bluebtnlg">Discard & Exit</a>
      </div>
    </div>
  </div>
</div>
  <!-- modal popup for exit confirmation of post a task -->



  


  



