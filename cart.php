<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header.php' ?>
  <!-- main -->
  <main class="subpage">
      <!-- apge header -->
      <div class="page-header">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-8">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a href="courses.php">Courses</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cart</li>
                        </ol>
                    </nav>
                    <h1>My Cart</h1>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
      </div>
      <!--/ page header -->

      <!-- pge body -->
      <div class="page-body">

      <!-- container -->
      <div class="container">
            <!-- row -->
            <div class="row">
                <!-- left col -->
                <div class="col-lg-9">
                    <h2 class="h4">3 Courses in your Cart</h2>

                    <!-- cart item-->
                    <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="javascript:void(0)">Mastering Microsoft Teams (2019)</a>
                                <span>Rs:360</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)">Remove</a>
                                <a href="javascript:void(0)">Save for Later</a>
                                <a href="javascript:void(0)">Move to Wishlist</a>
                            </p>
                        </article>
                    </div>
                    <!--/ cart item -->

                    <!-- cart item-->
                    <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="javascript:void(0)">Mastering Microsoft Teams (2019)</a>
                                <span>Rs:360</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)">Remove</a>
                                <a href="javascript:void(0)">Save for Later</a>
                                <a href="javascript:void(0)">Move to Wishlist</a>
                            </p>
                        </article>
                    </div>
                    <!--/ cart item -->

                    <!-- cart item-->
                    <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="javascript:void(0)">Mastering Microsoft Teams (2019)</a>
                                <span>Rs:360</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)">Remove</a>
                                <a href="javascript:void(0)">Save for Later</a>
                                <a href="javascript:void(0)">Move to Wishlist</a>
                            </p>
                        </article>
                    </div>
                    <!--/ cart item -->
                </div>
                <!--/ left col -->
                 <!-- left col -->
                 <div class="col-lg-3">

                     <h5 class="h5">Total</h5>
                     <h3 class="h3">Rs:1360 <span class="oldprice h6 fgray">Rs:2500</span></h3>
                     <h5 class="h5 fgray pb-4">85% Off</h5>
                     <p class="pb-3 mb-3 border-bottom">
                        <a href="javascript:void(0)" class="pinkbtnlg">CHECKOUT</a>
                     </p>

                     <!-- form -->
                     <form>
                         <div class="input-form d-flex">
                             <input type="text" placeholder="Enter Coupon">
                             <input type="submit" value="Submit">
                         </div>
                     </form>
                     <!--/ form -->
                 </div>
                <!--/ left col -->
            </div>
            <!--/ row -->

            <h2 class="h4 pt-3">You may also Like this</h2>
            <!--- row -->
            <div class="row other-getdone">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-3 col-sm-4">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course04.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                              <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                              <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->
            </div>
            <!--/ row -->
      </div>
      <!--/ container -->
           
      </div>
      <!--/ page bodyt -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
</body>

</html>