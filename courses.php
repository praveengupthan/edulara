<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header.php' ?>
  <!-- main -->
  <main class="subpage">
      <!-- apge header -->
      <div class="page-header" id="s0">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Courses</li>
                        </ol>
                    </nav>
                    <h1>Development Courses</h1>
                    <p>Best Online Courses for SSC, Railways & All other Govt. Exams</p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
      </div>
      <!--/ page header -->

      <!-- pge body -->
      <div class="page-body">
            <!--container -->
          <div class="container other-getdone">

           <!-- card row -->
           <div class="row">
                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course04.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course05.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                <!-- card -->
                <a href="javascript:void(0)">
                <div class="card">
                    <!-- card header -->
                    <div class="card-header">
                    <a href="javascript:void(0)"><img src="img/data/course08.jpg" alt="" class="img-fluid"></a>
                    <h4 class="h6"> 
                    <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                    </h4>
                    </div>
                    <!--/ card header -->
                    <!-- card body -->
                    <div class="card-body">
                    <p><small>Sundog Education by Frank Kane</small></p>
                    <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                    </div>
                    <!--/card card body -->
                    <!-- card footer -->
                    <div class="card-footer d-flex justify-content-between">
                    <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                    <div>
                        <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                        <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                    </div>
                    </div>
                    <!--/ card footer -->
                </div>
                <!--/ card -->
                </a>
            </div>
            <!--/ col -->

            <!-- col -->
            <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course04.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course05.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                    <!-- card -->
                    <a href="javascript:void(0)">
                    <div class="card">
                        <!-- card header -->
                        <div class="card-header">
                        <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                        <h4 class="h6"> 
                        <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                        </h4>
                        </div>
                        <!--/ card header -->
                        <!-- card body -->
                        <div class="card-body">
                        <p><small>Sundog Education by Frank Kane</small></p>
                        <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                        </div>
                        <!--/card card body -->
                        <!-- card footer -->
                        <div class="card-footer d-flex justify-content-between">
                        <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                        <div>
                            <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                            <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                        </div>
                        </div>
                        <!--/ card footer -->
                    </div>
                    <!--/ card -->
                    </a>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3 col-sm-6">
                <!-- card -->
                <a href="javascript:void(0)">
                <div class="card">
                    <!-- card header -->
                    <div class="card-header">
                    <a href="javascript:void(0)"><img src="img/data/course08.jpg" alt="" class="img-fluid"></a>
                    <h4 class="h6"> 
                    <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                    </h4>
                    </div>
                    <!--/ card header -->
                    <!-- card body -->
                    <div class="card-body">
                    <p><small>Sundog Education by Frank Kane</small></p>
                    <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                    </div>
                    <!--/card card body -->
                    <!-- card footer -->
                    <div class="card-footer d-flex justify-content-between">
                    <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                    <div>
                        <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                        <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                    </div>
                    </div>
                    <!--/ card footer -->
                </div>
                <!--/ card -->
                </a>
            </div>
            <!--/ col -->
            </div>
            <!--/ card row -->

          </div>
            <!--/ container -->
      </div>
      <!--/ page bodyt -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
</body>

</html>