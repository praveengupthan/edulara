<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header.php' ?>
  <!-- main -->
  <main class="subpage">
      <!-- apge header -->
      <div class="page-header">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-8">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a href="courses.php">Courses</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Mastering Microsoft Teams</li>
                        </ol>
                    </nav>
                    <h1>Mastering Microsoft Teams (2019)</h1>
                    <p>Learn How To Use Microsoft’s Communication & Collaboration Platform!</p>
                    <div class="d-flex">
                        <ul class="rating-tasker pr-3">
                            <li><span class="icon-star icomoon"></span></li>
                            <li><span class="icon-star icomoon"></span></li>
                            <li><span class="icon-star icomoon"></span></li>
                            <li><span class="icon-star icomoon"></span></li>
                            <li class="whiteicon"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p>
                            4.4 (6,142 ratings)
                            <span class="px-2 inline-block">|</span>
                            20,245 students enrolled
                        </p>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
      </div>
      <!--/ page header -->

      <!-- pge body -->
      <div class="page-body">

      <!-- container -->
      <div class="container">
          <!-- row -->
          <div class="row">
              <!-- col -->
              <div class="col-lg-8 col-sm-7">
                  <!-- gray box -->
                  <div class="graybox p-4">
                      <h2 class="h5 fsbold">What you will learn</h2>
                      <!-- row -->
                      <div class="row">
                          <!-- col -->
                          <div class="col-lg-6">
                              <ul class="list-item">
                                  <li>Describe the role and function of Microsoft Teams</li>
                                  <li>Describe the difference between Channel Conversations and Chat Conversations</li>
                                  <li>Discuss how to start scheduled or ad-hoc Meetings</li>
                                  <li>Add custom tabs to Channels and Chats</li>
                                  <li>Describe the function of Connectors</li>
                              </ul>
                          </div>
                          <!--/ col -->
                           <!-- col -->
                           <div class="col-lg-6">
                              <ul class="list-item">
                                  <li>Create and Manage Teams and Channels</li>
                                  <li>Identify uses for Chatbots</li>
                                  <li>Setup and Edit a Wiki</li>
                                  <li>Use Documents and Apps in Channels</li>
                                  <li>Discuss the use of external users (guests) in Teams, including limitations</li>
                              </ul>
                          </div>
                          <!--/ col -->
                      </div>
                      <!-- /row -->
                  </div>
                  <!-- gray box -->

                  <article class="pb-2">
                      <h3 class="h5">Requirements</h3>
                      <p>An account for Office 365 is recommended</p>
                  </article>

                  <article class="pb-2">
                      <h3 class="h5">Description</h3>
                      <h4 class="h6">Conversations, Channels, and Chatbots: Learn How To Get The Most from Microsoft’s New Communications Hub - Teams</h4>
                      <p>The ability for teams to work together productively is perhaps the most important function in any business, and it’s the central focus of the new Microsoft Teams application.  From file sharing and co-editing to video calls, persistent chat, screen sharing, and more, learn how Microsoft Teams gives you the tools to stay in touch and get work done with your colleagues and partners.</p>
                  </article>

                  <div class="py-2">
                    <h5 class="h5">Students also bought</h5>
                    <!-- row -->
                    <div class="row other-getdone">
                         <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <!-- card -->
                            <a href="javascript:void(0)">
                            <div class="card">
                                <!-- card header -->
                                <div class="card-header">
                                <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                                <h4 class="h6"> 
                                <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                                </h4>
                                </div>
                                <!--/ card header -->
                                <!-- card body -->
                                <div class="card-body">
                                <p><small>Sundog Education by Frank Kane</small></p>
                                <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                                </div>
                                <!--/card card body -->
                                <!-- card footer -->
                                <div class="card-footer d-flex justify-content-between">
                                <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                                <div>
                                    <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                                    <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                                </div>
                                </div>
                                <!--/ card footer -->
                            </div>
                            <!--/ card -->
                            </a>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <!-- card -->
                            <a href="javascript:void(0)">
                            <div class="card">
                                <!-- card header -->
                                <div class="card-header">
                                <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                                <h4 class="h6"> 
                                <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                                </h4>
                                </div>
                                <!--/ card header -->
                                <!-- card body -->
                                <div class="card-body">
                                <p><small>Sundog Education by Frank Kane</small></p>
                                <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                                </div>
                                <!--/card card body -->
                                <!-- card footer -->
                                <div class="card-footer d-flex justify-content-between">
                                <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                                <div>
                                    <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                                    <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                                </div>
                                </div>
                                <!--/ card footer -->
                            </div>
                            <!--/ card -->
                            </a>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                        <!-- card -->
                        <a href="javascript:void(0)">
                        <div class="card">
                            <!-- card header -->
                            <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course08.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                            </div>
                            <!--/ card header -->
                            <!-- card body -->
                            <div class="card-body">
                            <p><small>Sundog Education by Frank Kane</small></p>
                            <p><small><span class="icon-star fpink icomoon"></span> 4.5 (25)</small></p>
                            </div>
                            <!--/card card body -->
                            <!-- card footer -->
                            <div class="card-footer d-flex justify-content-between">
                            <a href="javascript:void(0)" class="like"><span class="icon-heart icomoon"></span></a>
                            <div>
                                <span class="small fgray oldprice"><span class="icon-inr icomoon"></span> 1200</span>
                                <span class="h6"><span class="icon-inr icomoon"></span> 750</span>
                            </div>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                        </a>
                    </div>
                    <!--/ col -->
                    </div>
                    <!--/ row -->
                  </div>

                  <!-- course content -->
                  <div class="course-content py-4">
                      <!-- title -->
                      <div class="d-flex justify-content-between">
                        <h3 class="h5">Course Content</h3>
                        <p class="text-right">04:58:43   <span class="seperator">|</span> 34 lectures</p>
                      </div>
                      <!--/ title -->

                      <!-- courses list -->
                       <!-- custom accord -->
                       <div class="accordion cust-accord">

                            <h3 class="panel-title">Course Content</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                        <span>08:46</span>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="panel-title">The Basics</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                        <span>08:46</span>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="panel-title">De Bugging</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                        <span>08:46</span>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="panel-title">Components and Data Binding</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                        <span>08:46</span>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /custom accord -->
                      <!--/ courses list -->
                  </div>
                  <!--/ course content -->

                  <!-- student feedback -->
                  <div class="feedback">
                    <h5 class="h5">Students Feedback</h5>

                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <ul class="rating-tasker pr-3 float-right">
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li class="blackicon"><span class="icon-star icomoon"></span></li>
                            </ul>
                         </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->

                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <ul class="rating-tasker pr-3 float-right">
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li class="blackicon"><span class="icon-star icomoon"></span></li>
                            </ul>
                         </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->

                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker04.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <ul class="rating-tasker pr-3 float-right">
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li class="blackicon"><span class="icon-star icomoon"></span></li>
                            </ul>
                         </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->

                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker03.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <ul class="rating-tasker pr-3 float-right">
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li class="blackicon"><span class="icon-star icomoon"></span></li>
                            </ul>
                         </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->

                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <ul class="rating-tasker pr-3 float-right">
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li><span class="icon-star icomoon"></span></li>
                                <li class="blackicon"><span class="icon-star icomoon"></span></li>
                            </ul>
                         </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->

                    <p class="text-center pt-5">
                        <a href="javascript:void(0)" class="bluebtnlg">SEE MORE REVIEWS</a>
                    </p>


                  </div>
                  <!--/ student feedback -->
              </div>
              <!--/ col -->
               <!-- col -->
               <div class="col-lg-4 col-sm-5">
                    <div class="graybox sticky-top">
                         <iframe width="100%" height="230" src="https://www.youtube.com/embed/_uQrJ0TkZlc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <div class="d-flex justify-content-between p-3 border-bottom">
                            <div>
                                <h5 class="h4 fbold">Rs:360 <span class="oldprice small">Rs:2500</span></h5>
                            </div>
                            <div>
                                <p class="fgrary text-right align-self-center">86% Off</p>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center p-3">
                            <a class="pinkbtnlg mx-2" href="javascript:void(0)">ADD TO CART</a>
                            <a class="bluebtnlg" href="javascript:void(0)">BUY NOW</a>
                        </div>
                        <p class="text-center fgray">30-Day Money-Back Guarantee</p>

                        <article class="p-3">
                        <h5 class="h5">This course includes</h5>
                            <ul class="list-item p-3">
                                <li>5 hours on-demand video</li>
                                <li>Full lifetime access</li>
                                <li>Access on mobile and TV</li>
                                <li>Certificate of Completion</li>
                            </ul>
                        </article>


                    </div>
               </div>
              <!--/ col -->
          </div>
          <!--/ row -->
      </div>
      <!--/ container -->
           
      </div>
      <!--/ page bodyt -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
</body>

</html>