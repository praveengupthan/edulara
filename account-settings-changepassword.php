<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Change Password</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 col-sm-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9 col-sm-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Change Password</h1>

                    <!-- row -->
                    <div class="row justify-content-between">
                        <!-- left col -->
                        <div class="col-lg-6">
                          
                           
                            <!-- form -->
                            <form class="custom-form">
                              <div class="form-group">
                                <label>Currnet Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" value="Praveen">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>New Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" value="Guptha">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Confirm Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" value="Guptha">
                                </div>
                              </div>

                              <div class="form-group">
                                <button class="bluebtnlg">Save Password</button>
                                <button class="pinkbtnlg">Cancel</button>
                              </div>  

                            </form>
                            <!--/ form -->                            
                        </div>
                        <!--/ left col -->

                       
                    </div>
                    <!--/ row -->                  

                 
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>