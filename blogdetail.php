<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Category as a Poster</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

  <!-- container -->
  <div class="container">
       <!-- row -->
       <div class="row py-3 justify-content-center">
          <!-- col -->
          <div class="col-lg-8">
              <h1 class="h1">
                    50+ Great garden edging ideas for your backyard
              </h1>
              <p>By Elise Hodge</p>
              <p class="fgray">Published: February 21st, 2020</p>

              <figure class="pb-1">
                  <img src="img/blog/blog01.jpg" alt="" class="img-fluid">
              </figure>
              <p>Garden edging is one of the most overlooked aspects of garden design – but it can make a huge difference to the overall appeal of your outdoor space. Whether you use it to define flower beds, create raised sections, pronounce garden paths or act as a border between pavers and grass, lawn edging is sure to add character to even the most simple of gardens. But it’s something that you might not realise that you can be a bit creative with, so let’s take a look at over 50 garden edging ideas.</p>

              <p>Having a garden border is more than just making your garden look pretty, it makes the surrounding grass easier to look after. This way you don’t need to worry about trimming around any awkward edges.</p>

              <h1 class="h3">
                    1. Bamboo edging
              </h1>
              <p>This is an eco-friendly option that not only looks beautiful but also protects your plants. Bamboo edging is a great option for a DIY garden project because it is flexible and easy to cut to size. This kind of edging is designed to be placed above the ground, so it prevents rotting and lasts longer!</p>

              <figure class="pb-1">
                  <img src="img/blog/blog02.jpg" alt="" class="img-fluid">
              </figure>

              <p>This may not be the first option you think of when you consider edging for your garden. But it’s a great way to upcycle unwanted pipes that wouldn’t be used otherwise. You can also add a little more detail by filling the pipes with stones or even planting succulents and cacti inside them. It’s great for a rustic garden </p>

              <p>If traditional is more your style, then you might want to go with classic terracotta tiling. This is a design that is always on trend because it perfectly highlights your gorgeous garden. Add some terracotta pots to your front porch or back deck, and you’ll have a consistent style throughout your outdoor space.</p>

              <h1 class="h3">
                    2. Pallets
              </h1>
              <p>Pallets are a great DIY building material, and with good reason – you can usually find them for free! Check Gumtree or ask local businesses for their leftover wood pallets. Cut them yourself, or get someone to do it for you, and then paint them white or any colour you desire. For a ‘vintage’ look, leave the wood slightly unfinished.</p>

              <p>This is an elegant choice that will last the test of time. Choose large pieces of stone to pull this look off, and fill in the gaps with sand to hold it all together!</p>

              <p>This is an effective garden solution that can also serve as a planter box for your herbs, flowers and vegetables. Use a single log or stack multiple logs to create height. </p>

              <p>This is a simple yet beautiful way to define your garden bed, border or path. Wrought iron edging not only adds a bit of pretty detail to your garden, but it’s also a practical addition to protect your garden and flower beds.</p>

          </div>
          <!--/ col -->
      </div>
      <!--/ row -->     
  </div>
  <!--/ container -->

    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>