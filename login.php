<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
  <!-- main -->
  <main class="login-main">
        <!-- left img -->
        <div class="leftimg">
            <img src="img/loginimg.jpg" alt="" class="img-fluid">
        </div>
        <!-- left img -->

        <!-- right login block -->
        <div class="right-login align-self-center custom-form">
           <h1 class="h4 text-center flight">Signin</h1>
            <!-- form -->
          <form>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <!-- form group -->
              <div class="form-group">
                  <label>Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <p class="text-right pb-2">
                <a href="javascript:void(0)" id="forgotpwlink" class="fblack">Forgot Password?</a>
              </p>
              <input type="submit" value="Login" class="w-100 pinkbtn">
          </form>
          <!--/ form -->

          
          <p class="text-center or py-2"><span>OR</span></p>
          <p class="text-center">Signin with your Social Networks</p>

          <div class="d-flex justify-content-between py-3">
              <a class="socialbtn fbbtn" href="javascript:void(0)">
                  <span class="icon-facebook icomoon"></span> 
                  Facebook
              </a>
              <a class="socialbtn googlesocial" href="javascript:void(0)">
                  <span class="icon-google-plus icomoon"></span> 
                  Google Plus
              </a>
          </div>

          <div class=" d-flex justify-content-between">
            <span>Don't have an account ?</span>
            <a href="javascript:void(0)" class="fpink">Sign up</a>
         </div>

        </div>
        <!--/ right login block -->
  </main>
  <!--/ main -->
  <?php include 'scripts.php' ?>
  

</body>

</html>