<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
  <!-- main -->
  <main class="login-main">
        <!-- left img -->
        <div class="leftimg">
            <img src="img/loginimg.jpg" alt="" class="img-fluid">
        </div>
        <!-- left img -->

        <!-- right login block -->
        <div class="right-login align-self-center custom-form">
           <h1 class="h4 text-center flight">Signup</h1>
            <!-- form -->
          <form>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
               <!-- form group -->
               <div class="form-group">
                  <label>User Name</label>
                  <div class="input-group">
                    <input type="text" placeholder="User Name" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <!-- form group -->
              <div class="form-group">
                  <label>Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
                <!-- form group -->
                <div class="form-group">
                  <label>Confirm Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Confirm Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
            
              <input type="submit" value="Signup" class="w-100 pinkbtn">
          </form>
          <!--/ form -->

        <div class=" d-flex justify-content-between pt-2">
            <span>Already have an Account ?</span>
            <a href="javascript:void(0)" class="fpink">Sign in</a>
         </div>

          <p class="text-center or py-2"><span>OR</span></p>
          <p class="text-center">Signin with your Social Networks</p>

          <div class="d-flex justify-content-between py-3">
              <a class="socialbtn fbbtn" href="javascript:void(0)">
                  <span class="icon-facebook icomoon"></span> 
                  Facebook
              </a>
              <a class="socialbtn googlesocial" href="javascript:void(0)">
                  <span class="icon-google-plus icomoon"></span> 
                  Google Plus
              </a>
          </div>
          <p>
          <span><input type="checkbox"></span>  
          Please don't send me tips or marketing via email or sms.
        </p>
        <p class="py-2">By signing up, I agree to Airtasker's 
          <a href="javascript:void(0)" class="fpink">Terms &amp; Conditions, </a>
          <a href="javascript:void(0)" class="fpink">Community Guidelines, </a>
          <a href="javascript:void(0)" class="fpink">Privacy Policy</a>
        </p> 
        </div>
        <!--/ right login block -->
  </main>
  <!--/ main -->
  <?php include 'scripts.php' ?>
  

</body>

</html>