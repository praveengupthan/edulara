<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body id="page-top">
    <?php include 'header.php' ?>
  <!-- main -->
  <main class="subpage">
      <!-- page header -->
      <div class="page-header pb-5 course-header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-8">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a href="courses.php">Courses</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Mastering Microsoft Teams</li>
                        </ol>
                    </nav>
                    <h1 class="pt-4 pt-sm-2">Big Data Architect Master's Course  <span class="bluebadge small">Master Program</span> </h1>
                    <div class="d-sm-flex d-block">
                       
                        <ul class="rating-tasker pr-3">
                            <li><span class="icon-star icomoon"></span></li>
                            <li><span class="icon-star icomoon"></span></li>
                            <li><span class="icon-star icomoon"></span></li>
                            <li><span class="icon-star icomoon"></span></li>
                            <li class="whiteicon"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p>
                            4.4 (6,142 ratings)
                            <span class="px-2 inline-block">|</span>
                            20,245 Learners
                        </p>
                    </div>
                    <p>Our Big Data Architect master's course lets you gain proficiency in Big Data. You will work on real-world projects in Hadoop Development, Hadoop Administration, Hadoop Analysis, Hadoop Testing, Spark, Python, Splunk Developer and Admin, Apache Storm, NoSQL databases and more. In this program, you will cover 12 courses and 31 industry-based projects. As a part of this online classroom training, you will receive four additional self-paced courses co-created with IBM, namely, Spark Fundamentals I and II, Spark MLlib, and Python for Data Science.</p>

                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-8">
                        <p>In Collaboration with <img src="img/ibmpng.png" style="width:50px;"></p>
                            <div class="d-flex py-1">
                                <div class="px-3">
                                    <p class="text-center h4 pb-0">10</p>
                                    <p class="text-center small">Courses</p>
                                </div>
                                <div class="px-3 border-left border-right">
                                    <p class="text-center h4 pb-0">53</p>
                                    <p class="text-center small">Projects</p>
                                </div>
                                <div class="px-3">
                                    <p class="text-center h4 pb-0">232</p>
                                    <p class="text-center small">Hours</p>
                                </div>                                  
                            </div>
                            <a class="pinkbtnlg text-uppercase d-inline-block mb-3" href="javascript:void(0)">ENROLL NOW</a>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4">
                            <div class="linktext d-flex">
                                <!-- <h6 class="align-self-end pr-4 fblue">Watch Video</h6> -->
                                <div class="video-testimonil-link" data-toggle="modal" data-target=".student-testimonial">
                                    <a href="javascript:void(0)"> 
                                        <span class="icon-play icomoon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>                   
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4">
                    <div class="feat MCSDetails">
                        <ul>
                            <li>
                                <div class="MCSHead"><p class="MCSHead-heading">Online Classroom Training</p></div>
                                <ul>
                                    <li class="mcsdetailslist" data-id="1">Big Data Hadoop &amp; Spark</li>
                                    <li class="mcsdetailslist" data-id="2"> Apache Spark &amp; Scala</li>
                                    <li class="mcsdetailslist" data-id="3"> Splunk Developer &amp; Admin</li>
                                    <li class="mcsdetailslist" data-id="4"> Python for Data Science</li>
                                    <li class="mcsdetailslist" data-id="5"> Pyspark</li>
                                    <li class="mcsdetailslist" data-id="6"> MongoDB</li>
                                </ul>
                            </li>
                            <li>
                                <div class="MCSHead"><p class="MCSHead-heading">Self Paced Training</p></div>
                                <ul>
                                    <li class="mcsdetailslist" data-id="7">Hadoop Testing&nbsp;</li>
                                    <li class="mcsdetailslist" data-id="8"> Apache Storm&nbsp;</li>
                                    <li class="mcsdetailslist" data-id="9"> Apache Kafka&nbsp;</li>
                                    <li class="mcsdetailslist" data-id="10"> Apache Cassandra&nbsp;</li>
                                    <li class="mcsdetailslist" data-id="11"> Java&nbsp;</li>
                                    <li class="mcsdetailslist" data-id="12"> Linux&nbsp;</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
      </div>
      <!--/ page header -->

      <!-- pge body -->
      <div class="page-body pt-0">
      <!-- Navigation -->
        <nav class="navbar navbar-expand-lg" id="mainNav">
            <div class="container">    
                <a class="navbar-brand h6 flight fgray" href="#">Course Specifications</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#keyFeatures">Key Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#CourseFee">Course Fee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#overview">Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#testimonials">Testimonials</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#CourseContent">Course Content</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#Certification">Certification</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#CourseAdvisor">Course Advisor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#faq">Faq</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>  
        <!--/ Navigation -->

        <!-- key features -->
        <div id="keyFeatures" class="scrollSection">
            <!-- container -->
            <div class="container">
                <h2 class="h4">Key Features</h2>

                <ul class="row horitems">
                    <li class="col-lg-3"><span class="icon-professions-and-jobs icomoon"></span> 173 Hrs Instructor Led Training</li>
                    <li class="col-lg-3"><span class="icon-video icomoon"></span>277 Hrs Self-paced Videos</li>
                    <li class="col-lg-3"><span class="icon-project icomoon"></span> 384 Hrs Project work & Exercises</li>
                    <li class="col-lg-3"><span class="icon-certificate icomoon"></span>Certification and Job Assistance</li>
                    <li class="col-lg-3"><span class="icon-alarm-clock icomoon"></span>Flexible Schedule</li>
                    <li class="col-lg-3"><span class="icon-infinity icomoon"></span>Lifetime Free Upgrade</li>
                    <li class="col-lg-3"><span class="icon-h icomoon"></span>24 x 7 Lifetime Support & Access</li>
                </ul>            
            </div>
            <!--/ container -->
        </div>
        <!--/ key features -->

        <!-- course fee-->
        <div id="CourseFee" class="scrollSection bg-section">
            <div class="container">
                <h2 class="h4">Course Fees</h2>

                <!-- white box -->
                <div class="whitebox ">
                    <!-- row -->
                    <div class="row justify-content-between">
                    <div class="col-lg-9">
                        <h6 class="h6">Self Paced Training</h6>
                        <ul class="list-item d-flex flex-wrap small-item mb-0">
                            <li class="small">277 Hrs e-learning video</li>
                            <li class="small">Lifetime Free Upgrade</li>
                            <li class="small">24 x 7 Lifetime Support & Access</li>
                            <li class="small">Flexi-scheduling</li>
                        </ul>
                    </div>
                    <div class="col-lg-3">
                        <h5 class="h5 text-right pb-2"><span class="icon-inr"></span> 30,039</h5>
                        <p class="text-right">
                            <a class="bluebtnlg" href="javascript:void(0)">Enroll Now</a>
                        </p>
                    </div>
                    </div>
                    <!--/row -->
                </div>
                <!--/ white box -->

            <!-- white box -->
            <div class="whitebox">
                <!-- row -->
                <div class="row">
                <!-- col -->
                <div class="col-lg-8">
                    <h6 class="h6">Online Classroom <span class="bluebadge small">PREFERRED</span></h6>
                    <ul class="list-item d-flex flex-wrap small-item">
                        <li class="small">Everything in self-paced, plus</li>
                        <li class="small">173 Hrs of instructor-led training</li>
                        <li class="small">1:1 doubt resolution sessions</li>
                        <li class="small">Attend as many batches for Lifetime</li>
                        <li class="small">Flexible Schedule</li>
                    </ul>
                    </div>
                    <!--/ col -->
                    </div>
                    <!-- row-->

                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-8">
                        <!-- option-->
                        <div class="ONOptiop">
                            <form>
                                <div class="class-timetable online-classroom-product checked">
                                    <input type="radio">
                                    <ul class="mb-0">
                                        <li>
                                            <span class="checked-icon"></span>
                                        </li>
                                        <li><span class="ONOptiopdate">11 Jul</span></li>
                                        <li>
                                            <span class="ONOptiopday">SAT - SUN</span>
                                            <span class="ONOptiopcsl"></span>
                                        </li>
                                        <li>
                                            <span class="ONOptioptime">08:00 PM TO 11:00 PM IST (GMT +5:30)</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="class-timetable online-classroom-product">
                                    <input type="radio">
                                    <ul class="mb-0">
                                        <li>
                                            <span class="checked-icon"></span>
                                        </li>
                                        <li><span class="ONOptiopdate">14 Jul</span></li>
                                        <li>
                                            <span class="ONOptiopday">TUE - FRI</span>
                                            <span class="ONOptiopcsl"></span>
                                        </li>
                                        <li>
                                            <span class="ONOptioptime">08:00 PM TO 11:00 PM IST (GMT +5:30)</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="class-timetable online-classroom-product">
                                <input type="radio">
                                    <ul class="mb-0">
                                        <li>
                                            <span class="checked-icon"></span>
                                        </li>
                                        <li><span class="ONOptiopdate">18 Jul</span></li>
                                        <li>
                                            <span class="ONOptiopday">SAT - SUN</span>
                                            <span class="ONOptiopcsl"></span>
                                        </li>
                                        <li>
                                            <span class="ONOptioptime">08:00 PM TO 11:00 PM IST (GMT +5:30)</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="class-timetable online-classroom-product">
                                <input type="radio">
                                    <ul class="mb-0">
                                        <li>
                                            <span class="checked-icon"></span>
                                        </li>
                                        <li><span class="ONOptiopdate">25 Jul</span></li>
                                        <li>
                                            <span class="ONOptiopday">SAT - SUN</span>
                                            <span class="ONOptiopcsl"></span>
                                        </li>
                                        <li>
                                            <span class="ONOptioptime">08:00 PM TO 11:00 PM IST (GMT +5:30)</span>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>                    
                        <!-- /options-->
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-4 text-right">
                        <h6 class="h6 oldprice fgray"><span class="icon-inr"></span>72,881</h6>
                        <h5 class="h4"><span class="icon-inr"></span>48,051</h5>
                        <p class="small fblue text-right">10% OFF Expires in 00d 23h 35m 34s </p>
                        <p class="text-right mt-4">
                            <a class="bluebtnlg" href="javascript:void(0)">Enroll Now</a>
                        </p>
                        <p class="small text-right">No Interest Financing start at ₹ 5000 / month</p>
                    </div>
                    <!-- col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ white box -->

            <!-- white box -->
            <div class="whitebox">
                <!-- row -->
                <div class="row justify-content-between">
                    <div class="col-lg-9">
                        <h6 class="h6">Corporate Training</h6>
                        <ul class="list-item d-flex flex-wrap small-item mb-0">
                            <li class="small">Customized Learning</li>
                            <li class="small">Enterprise grade learning management system (LMS)</li>
                            <li class="small">24x7 support</li>
                            <li class="small">Strong Reporting</li>
                        </ul>
                    </div>
                    <div class="col-lg-3 align-self-center">                    
                        <p class="text-right">
                            <a class="bluebtnlg" href="javascript:void(0)">Contact us</a>
                        </p>
                    </div>
                </div>
                <!--/ row -->                
            </div>
            <!--/ white box -->
            </div>
        </div>
        <!-- /course fee-->

        <!-- overview -->
        <div id="overview" class="scrollSection">
            <!-- container -->
            <div class="container">
            <h2 class="h4">Big Data Architect Overview</h2>

            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-8">
                    <p>Intellipaat’s Big Data Architect master’s course will provide you with in-depth knowledge on Big Data platforms like Hadoop, Spark and NoSQL databases, along with a detailed exposure of analytics and ETL by working on tools. This program is specially designed by industry experts, and you will get 12 courses with 31 industry-based projects.</p>

                     <!-- courses list -->
                       <!-- custom accord -->
                       <div class="accordion cust-accord">

                            <h3 class="panel-title">List of Courses Included:</h3>
                            <div class="panel-content">
                                <h6 class="h6">Online Instructor-led Courses:</h6>
                                <ul class="list-item">
                                    <li>Big Data Hadoop and Spark</li>
                                    <li>Apache Spark and Scala</li>
                                    <li>Splunk Developer and Admin</li>
                                    <li>Python for Data Science</li>
                                    <li>Pyspark Training</li>
                                    <li>MongoDB</li>
                                </ul>
                                <h6 class="h6">Self-paced Courses:</h6>
                                <ul class="list-item">
                                    <li>Big Data Hadoop and Spark</li>
                                    <li>Apache Spark and Scala</li>
                                    <li>Splunk Developer and Admin</li>
                                    <li>Python for Data Science</li>
                                    <li>Pyspark Training</li>
                                    <li>MongoDB</li>
                                </ul>

                            </div>

                            <h3 class="panel-title">What will you learn in this master's course?</h3>
                            <div class="panel-content">                           
                                <ul class="list-item">
                                    <li>Introduction to Hadoop ecosystem</li>
                                    <li>Working with HDFS and MapReduce</li>
                                    <li>Real-time analytics with Apache Spark</li>
                                    <li>ETL in Business Intelligence domain</li>
                                    <li>Working on large amounts of data with NoSQL databases</li>
                                    <li>Real-time message brokering system</li>
                                    <li>Hadoop analysis and testing</li>
                                </ul>
                            </div>

                            <h3 class="panel-title">Who should take up this training?</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                        <span>08:46</span>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="panel-title">What are the prerequisites for taking up this training course?</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                        <span>08:46</span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                        <span>08:46</span>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="panel-title">Why should you take up this training program?</h3>
                            <div class="panel-content">
                                <ul class="videos-list">
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                       
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                       
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                       
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                       
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                       
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /custom accord -->
                      <!--/ courses list -->                      
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-lg-4">
                     <div class="card">
                         <div class="card-header bluebg">
                             <h4 class="h4">Talk To Us</h4>
                         </div>
                         <div class="card-body">
                             <p>IN : +91-7022374614</p>
                             <p>US : 1-800-216-8930 (Toll Free)</p>
                         </div>
                     </div>
                 </div>
                <!--/ col -->
            </div>
            <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ overview -->

        <!-- testimonials -->
        <div id="testimonials" class="scrollSection bg-section">
             <!-- container fluid -->
            <div class="container">
                <h2 class="h4">Testimonials</h2>

                <!-- slider video testimonials -->
                <div class="custom-slick video-testimonials">
                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- card -->
                    <div class="card tasker-item">
                    <!-- card body -->
                    <div class="card-body text-center p-0 video-test">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial">
                                <figure class="position-relative m-0">
                                    <img src="img/videotestimg.jpg" alt="" class="img-fluid">
                                    <span class="icon-play-circle icomoon position-absolute"></span>
                                </figure>
                            </a>
                    </div>
                    <!--/ card body -->
                    <!-- card footer -->
                    <div class="card-footer text-center">
                        <p class="pb-0">
                            Name will be here                  
                        </p>
                    </div>
                    <!--/ card footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ slide -->
                </div>
                <!-- slider video testimonials -->

                <!-- slider text testimonials -->
                <div class="custom-slick text-testimonials">
                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker04.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                   <!-- feedback row -->
                   <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker03.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                      
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">6 days ago</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->             
            </div>
            <!-- slider text testimonials -->               
            </div>
            <!--/ container  -->
        </div>     
        <!--/ testimonials -->  

        <!-- course content -->
        <div id="CourseContent" class="scrollSection">
            <div class="container">
               

                <!-- row -->
               <div class="row">
                   <!-- col -->
                   <div class="col-lg-8">
                    <div class="d-block d-sm-flex justify-content-between">
                        <h2 class="h4">Course Content</h2>
                        <button class="pinkbtnlg align-itemns-center mt-0 mb-2">Download Curriculum</button>
                    </div>

                      <!-- custom accord -->
                      <div class="accordion cust-accord">

                    <h3 class="panel-title">List of Courses Included:</h3>
                    <div class="panel-content">
                        <h6 class="h6">Online Instructor-led Courses:</h6>
                        <ul class="list-item">
                            <li>Big Data Hadoop and Spark</li>
                            <li>Apache Spark and Scala</li>
                            <li>Splunk Developer and Admin</li>
                            <li>Python for Data Science</li>
                            <li>Pyspark Training</li>
                            <li>MongoDB</li>
                        </ul>
                        <h6 class="h6">Self-paced Courses:</h6>
                        <ul class="list-item">
                            <li>Big Data Hadoop and Spark</li>
                            <li>Apache Spark and Scala</li>
                            <li>Splunk Developer and Admin</li>
                            <li>Python for Data Science</li>
                            <li>Pyspark Training</li>
                            <li>MongoDB</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>

                    <h3 class="panel-title">What will you learn in this master's course?</h3>
                    <div class="panel-content">                           
                        <ul class="list-item">
                            <li>Introduction to Hadoop ecosystem</li>
                            <li>Working with HDFS and MapReduce</li>
                            <li>Real-time analytics with Apache Spark</li>
                            <li>ETL in Business Intelligence domain</li>
                            <li>Working on large amounts of data with NoSQL databases</li>
                            <li>Real-time message brokering system</li>
                            <li>Hadoop analysis and testing</li>
                        </ul>
                    </div>
                    </div>
                    <!-- /custom accord -->

                   </div>
                   <!--/ col -->
                     <!-- right col -->
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header bluebg ">
                        <h5>Free Career Counselling</h5>
                        <p class="small">We are happy to help you</p>
                    </div>
                    <div class="card-body">
                        <formn>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Mobile Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                   <textarea class="form-control" style="height:120px;" placeholder="Write Message"></textarea>
                                </div>
                            </div>
                            <button class="pinkbtnlg">Send Message</button>
                        </formn>
                    </div>
                </div>
            </div>
            <!--/ right col -->
               </div>
                <!--/ row -->
            </div>
        </div>
        <!-- course content -->

        <!-- certifications -->
        <div id="Certification" class="scrollSection bg-section">
            <div class="container">
                <h2 class="h4">Big Data Architect Certification</h2>
                <p>This is a comprehensive course that is designed to clear multiple certifications such as:</p>

                <ul class="list-item">
                    <li>CCA Spark and Hadoop Developer (CCA175)</li>
                    <li>Splunk Certified Power User Certification</li>
                    <li>Splunk Certified Admin Certification</li>
                    <li>Apache Cassandra DataStax Certification</li>
                    <li>Linux Foundation Linux Certification</li>
                    <li>Java SE Programmer Certification</li>
                </ul>
                <p>Furthermore, you will also be rewarded as a “Big Data Professional” for completing the following learning path that are co-created with IBM:</p>

                <ul class="list-item">
                    <li>Spark Fundamentals I</li>
                    <li>Spark MLlib</li>
                    <li>Spark Fundamentals II</li>
                    <li>Python for Data Science</li>                   
                </ul>
                <p>The complete course is created and delivered in association with IBM to get top jobs in the world’s best organizations. The entire training includes real-world projects and case studies that are highly valuable.</p>

                <p>As part of this training, you will be working on real-time projects and assignments that have immense implications in the real-world industry scenario, thus helping you fast-track your career effortlessly.</p>

                <p>At the end of this training program, there will be quizzes that perfectly reflect the type of questions asked in the respective certification exams and help you score better marks.</p>

                <p>Intellipaat Course Completion Certificate will be awarded upon the completion of project work (after expert review) and upon scoring at least 60% marks in the quiz. Intellipaat certification is well recognized in top 80+ MNCs like Ericsson, Cisco, Cognizant, Sony, Mu Sigma, Saint-Gobain, Standard Chartered, TCS, Genpact, Hexaware, etc.</p>

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/certificate01.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-6">
                        <img src="img/certificate02.jpg" alt="" class="img-fluid">
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

            </div>
        </div>
        <!-- /certifications -->

        <!-- course advisor -->
        <div id="CourseAdvisor" class="scrollSection">
            <!-- container -->
            <div class="container">
            <h2 class="h4">Course Advisor</h2>

             <!-- Course Advisor -->
             <div class="custom-slick text-testimonials">
                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">Solutions Architect at Microsoft, USA</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">Solutions Architect at Microsoft, USA</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker04.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">Solutions Architect at Microsoft, USA</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                   <!-- feedback row -->
                   <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker03.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">Solutions Architect at Microsoft, USA</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">Solutions Architect at Microsoft, USA</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                      
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <!-- feedback row -->
                    <div class="row feedbackrow">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb">
                                <article class="pl-3 align-self-center">
                                    <h6 class="h6 pb-0 mb-0">Praveen  Guptha N</h6>
                                    <p><small class="fgray">Solutions Architect at Microsoft, USA</small></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                       
                        <!-- col -->
                        <div class="col-lg-12">
                            <p class="pb-0">Very helpful. Much of the information here, I would not have been able to know about without this assistance. I am interested in knowing when updates to this lesson are made so I can have the most up-to-date information as software can change frequently/daily.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ feedback row -->
                </div>
                <!--/ slide -->             
            </div>
            <!-- Course Advisor -->     

           
            </div>
            <!--/ contrainer -->
        </div>
        <!-- course advisor -->


        <!-- faq -->
        <div id="faq" class="scrollSection bg-section">
            <div class="container">
                <h2 class="h4">Faq's</h2>

                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-8">

                     <!-- custom accord -->
                     <div class="accordion cust-accord">
                        <h3 class="panel-title">What Is Intellipaat’s Masters Course And How It Is Different From Individual Courses?</h3>
                        <div class="panel-content">
                        <p>Intellipaat’s Masters Course is a structured learning path specially designed by industry experts which ensures that you transform into Big Data expert. Individual courses at Intellipaat focus on one or two specializations. However, if you have to masters big data then this program is for you</p>
                        </div>

                        <h3 class="panel-title">What Is Intellipaat’s Masters Course And How It Is Different From Individual Courses?</h3>
                        <div class="panel-content">                           
                            <p>Intellipaat’s Masters Course is a structured learning path specially designed by industry experts which ensures that you transform into Big Data expert. Individual courses at Intellipaat focus on one or two specializations. However, if you have to masters big data then this program is for you</p>
                        </div>

                        <h3 class="panel-title">Why Should I Learn Big Data Architect From Intellipaat?</h3>
                        <div class="panel-content">
                        <p>Intellipaat is the pioneer of Big Data Architect training we provide:</p>
                        <ul class="list-item">
                            <li>Project work &amp; Assignment – You will work on 28 industry based project which will give you hands on experience on the technology
                            </li>
                            <li>24*7 Support – Our Team work 24*7 to clear all your doubts</li>
                            <li>Free Course Upgrade – Keep yourself updated with latest version hence it’s a lifetime investment at one go</li>
                            <li>Flexible Schedule –You can attend as many batches as you want or if you are busy then you can postpone your classes to our next available batches without any extra charges.
                            </li>
                            <li>Resume Preparation &amp; Job Assistance –We will help you to prepare your resume and market your profile for jobs. We have more than 80 clients across the globe (India, US, UK, etc.) and we circulate our learner’s profiles to them.</li>
                        </ul>
                        </div>

                        <h3 class="panel-title">What Are The Various Modes Of Training That Intellipaat Offers?</h3>
                        <div class="panel-content">
                        <p>Intellipaat offers the self-paced training and online instructor-led training.</p><p>Hadoop developer, Hadoop admin, Hadoop analyst, Hadoop testing, Spark &amp; Scala, Python, Splunk Developer &amp; Admin and&nbsp; Java, Apache Storm, Apache Cassandra, Apache kafka are self-paced courses</p>
                        </div>

                        <h3 class="panel-title">What Is The System Requirement To Attend The Training?</h3>
                        <div class="panel-content">
                        <p>Preferably 8 GB RAM (Windows or Mac) with a good internet connection</p>
                        </div>

                        </div>
                        <!-- /custom accord -->

                    </div>
                    <!--/ left col -->
                     <!-- rt col -->
                     <div class="col-lg-4">
                     <div class="card">
                         <div class="card-header bluebg">
                             <h4 class="h4">Talk To Us</h4>
                         </div>
                         <div class="card-body">
                             <p>IN : +91-7022374614</p>
                             <p>US : 1-800-216-8930 (Toll Free)</p>
                         </div>
                     </div>
                     </div>
                    <!--/ rt col -->
                </div>
                <!--/ row -->
               
            </div>
        </div>      
        <!-- /faq -->       
           
      </div>
      <!--/ page body -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>

  <!-- Modal video -->
  <div class="modal fade video-modal student-testimonial"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">               
                    <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe id="ytplayer" class="modalvideo" width="100%" src="https://www.youtube.com/embed/HSgjpQBkR0c"  allow="autoplay" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->


    <script>
   
    $('.video-modal').on('hidden.bs.modal', function (e) {
    $('.video-modal iframe').attr('src', '');
    });

    $('.video-modal').on('show.bs.modal', function (e) {
        $('.video-modal iframe').attr('src', 'https://www.youtube.com/embed/HSgjpQBkR0c?rel=0&amp;autoplay=1');
    });
    </script>
</body>

</html>