<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Payment Methods</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 col-sm-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9 col-sm-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Payment Methods</h1>

                     <!-- tab -->
                    <div class="custom-tab">
                    
                          <ul class="nav nav-pills" id="myTab" role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link active" id="make-payment-tab" data-toggle="tab" href="#makepayment" role="tab" aria-controls="home" aria-selected="true">My Payments</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" id="receivepayment-tab" data-toggle="tab" href="#receivepayment" role="tab" aria-controls="profile" aria-selected="false">Billing Address</a>
                              </li>                           
                          </ul>

                          <div class="tab-content pt-3" id="myTabContent">
                              <!--  Make Payment -->
                              <div class="tab-pane fade show active" id="makepayment" role="tabpanel" aria-labelledby="make-payment-tab">
                                <!-- row -->
                                <div class="row pt-3 border-top">
                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <h6 class="h6">Add your Card</h6>
                                            <div class="py-3">
                                            <p>When you are ready to accept a Tasker's offer, you will be required to pay for the task using Airtasker Pay. Payment will be held securely until the task is complete and you release payment to the Tasker.</p>
                                                
                                            <a href="javascript:void(0)" class="fblue" data-toggle="modal" data-target="#addcard">
                                                <span class="icon-plus-circle icomoon"></span> Add your credit card
                                            </a>

                                            <h6 class="h6 mt-5">Payment history</h6>
                                           <div class="table-responsive">
                                             <!-- table -->
                                             <table class="table ">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th scope="col">Course</th>
                                                        <th scope="col">Date of Purchase</th>
                                                        <th scope="col">Cost </th>
                                                        <th scope="col">Payment Method</th>
                                                        <th scope="col">Invoice</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Angular - The Complete Guide (2020 Edition)
                                                        </th>
                                                        <td>22-11-2019</td>
                                                        <td>Rs: 2500</td>
                                                        <td>Visa Card Payment</td>
                                                        <td>
                                                            <a href="javascript:void(0)"><span class="icon-download"></span></a>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <th scope="row">The Complete JavaScript Course 2020: Build Real Projects!
                                                        </th>
                                                        <td>10-11-2019</td>
                                                        <td>Rs: 2500</td>
                                                        <td>Visa Card Payment</td>
                                                        <td>
                                                            <a href="javascript:void(0)"><span class="icon-download"></span></a>
                                                        </td>
                                                    </tr>       
                                                    <tr>
                                                        <th scope="row">AAdvanced CSS and Sass: Flexbox, Grid, Animations and More!
                                                        </th>
                                                        <td>15-11-2019</td>
                                                        <td>Rs: 2500</td>
                                                        <td>Visa Card Payment</td>
                                                        <td>
                                                            <a href="javascript:void(0)"><span class="icon-download"></span></a>
                                                        </td>
                                                    </tr>                                                         
                                                </tbody>
                                            </table>
                                            <!--/ table -->
                                           </div>
                                    </div>
                                        
                                    </div>
                                    <!--/ col --> 
                                </div>
                                <!--/ row -->

                              </div>
                              <!--/ Make Payment -->

                              <!-- Received Payment -->
                              <div class="tab-pane fade" id="receivepayment" role="tabpanel" aria-labelledby="receivepayment-tab">
                                <!-- row -->
                                <div class="row pt-3 border-top"> 

                                    <div class="col-lg-12"> 
                                        <h6 class="h6">Billing Address</h6>
                                        <p>Your billing address will be verified before you can receive payments.</p>
                                    </div>

                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Address Line 1</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Address Line 1">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Address Line 2 (optional)</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Address Line 2">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Suburb</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Suburb">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="State">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Postcode</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Postcode">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Country">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-12 pt-3">
                                        <a class="bluebtnlg" href="javascript:void(0)">Add Billing Address</a>
                                       <p class="pt-2"> <small>Your address will never been shown publicly, it is only used for account verification purposes.</small></p>
                                    </div>
                                    <!--/ col -->
                                   
                                </div>
                                <!--/ row -->

                              </div>
                              <!--/Received Payment -->
                          </div>

                      </div>

                      <!--/ tab -->

                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->


<!-- Modal -->
<div class="modal fade" id="addcard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add your credit card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Card Number</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Enter Card Number">
                    </div>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6">
                <div class="form-group">
                    <label>Expiry Date</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="MM/YY">
                    </div>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6">
                <div class="form-group">
                    <label>CVC</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="CVC">
                    </div>
                </div>
            </div>
            <!--/ col -->

            <div class="col-lg-12">
                <p class="small">By providing your credit card details, you agree to Stripe's Terms & Conditions.</p>
            </div>

        </div>
        <!--/ row -->
      </div>
      <div class="modal-footer">
        <button type="button" class="pinkbtnlg" data-dismiss="modal">Cancel</button>
        <button type="button" class="bluebtnlg">Save Card</button>
      </div>
    </div>
  </div>
</div>

  <?php include 'scripts.php' ?> 


</body>
</html>