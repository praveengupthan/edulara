<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Category as a Poster</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

  <!-- container -->
  <div class="container">
       <!-- row -->
       <div class="row py-3">
          <!-- col -->
          <div class="col-lg-6">
              <h1 class="h1">
                    Helping you get more done
              </h1>
              <p>WE FOUND 473 ARTICLES FOR YOU</p>
          </div>
          <!--/ col -->
      </div>
      <!--/ row -->
      <!-- row -->
      <div class="row">
          <!-- col -->
          <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog01.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">50+ Great garden edging ideas for your backyard</h3>
                        <p>Make your garden stand out with defined edging!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog02.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Emma Warren</p>
                        <h3 class="h5">How to clean silver quickly and effectively</h3>
                        <p>Here are some easy peasy ways to clean your silver</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog03.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Gianna Huesch</p>
                        <h3 class="h5">Beautiful bathroom shower ideas for your home</h3>
                        <p>You'll never want to get out of your shower if they looked like this...</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog04.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Gianna Huesch</p>
                        <h3 class="h5">The best indoor and hanging bathroom plants</h3>
                        <p>Bask in nature by bringing greenery indoors with lush bathroom plants</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog05.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">50+ Great garden edging ideas for your backyard</h3>
                        <p>Make your garden stand out with defined edging!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog06.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">50+ Master bathroom ideas</h3>
                        <p>Create the peaceful oasis of your dreams!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog07.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">50+ Great garden edging ideas for your backyard</h3>
                        <p>Make your garden stand out with defined edging!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog08.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Gianna Huesch</p>
                        <h3 class="h5">40 Beautiful bathroom wallpaper ideas</h3>
                        <p>Is your bathroom lacking character? Add instant personality with wallpaper!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog09.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">49+ Bathroom decor ideas</h3>
                        <p>What’s ahead for bathroom decor ideas in 2020?</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog10.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">50+ Great garden edging ideas for your backyard</h3>
                        <p>Make your garden stand out with defined edging!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog11.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">35+ Clever bathroom storage ideas</h3>
                        <p>Goodbye clutter - it's time to make sure everything has a place!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->

           <!-- col -->
           <div class="col-lg-4 col-sm-6">
                <div class="blog-col">
                    <a href="blogdetail.php">
                        <img src="img/blog/blog01.jpg" alt="" class="img-fluid">
                    </a>
                    <article>
                        <p class="author">By Elise Hodge</p>
                        <h3 class="h5">50+ Great garden edging ideas for your backyard</h3>
                        <p>Make your garden stand out with defined edging!</p>
                    </article>
                </div>
          </div>
          <!--/ col -->
      </div>
      <!--/ row -->
  </div>
  <!--/ container -->

    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>