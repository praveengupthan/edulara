<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header.php' ?>

  <!-- main -->
  <main>
    <!-- main slider -->
    <div class="homeslider d-flex justify-content-center align-items-center">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <!-- col 8-->
          <div class="col-lg-8">
            <h1 class="h1 fbold">The world’s largest selection of online courses</h1>
            <p>Everyone gets a full induction when they start, including any optional training relevant to your role such as fire safety and first.</p>
            <a href="javascript:void(0)">Get Started Now</a>
          </div>
          <!--/ col 8-->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ main slider -->

    <!-- highlights -->
    <div class="highlights">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <!--/ col -->
          <div class="col-sm-4  highletcol">
              <div class="d-flex">
                <div class="highlets-icon"><span class="icon-interface-1 icomoon"></span></div>
                <article>
                    <h5>100,000 online courses</h5>
                    <p>Explore a variety of fresh topics</p>
                </article>
              </div>
          </div>
          <!--/ col -->
           <!--/ col -->
           <div class="col-sm-4 highletcol">
              <div class="d-flex">
                <div class="highlets-icon"><span class="icon-education1 icomoon"></span></div>
                <article>
                    <h5>Expert instruction</h5>
                    <p>Find the right instructor for you</p>
                </article>
              </div>
          </div>
          <!--/ col -->
           <!--/ col -->
           <div class="col-sm-4 highletcol">
              <div class="d-flex">
                <div class="highlets-icon"><span class="icon-future icomoon"></span></div>
                <article>
                    <h5>Lifetime access</h5>
                    <p>Learn on your schedule</p>
                </article>
              </div>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ highlights -->

    <!-- categories -->
    <div class="home-categories home-section">
      <!-- container -->
      <div class="container">
        <h2 class="pb-2 text-center">Courses Categories</h2>

        <div class="categories-in d-flex justify-content-lg-center">
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-business icomoon"></span>
            <span class="text">Banking & Finance</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-miscellaneous icomoon"></span>
            <span class="text">Accounts</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-computing icomoon"></span>
            <span class="text">IT & Software </span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-Gears-Cutting icomoon"></span>
            <span class="text">Engineering</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-education icomoon"></span>
            <span class="text">Education & Teaching</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-business-1 icomoon"></span>
            <span class="text">Civil Exams</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-camera icomoon"></span>
            <span class="text">Photography</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-search icomoon"></span>
            <span class="text">More Categories</span>
          </a>
          <!--/ col -->
        </div>

      </div>
      <!--/ container -->
    </div>
    <!--/ categories -->

    <!-- see others get done -->
    <div class="other-getdone graybox">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="pb-2">The world’s largest selection of courses</h2>
            <p>Choose from over 100,000 online video courses with new additions published every month</p>
          </div>
        </div>
        <!-- row -->

        <!-- row -->
        <div class="row">
          <!-- col -->
          <div class="col-lg-12">
            <!-- tab -->
            <div class="parentHorizontalTab customtab">
              <ul class="resp-tabs-list hor_1 nav justify-content-center">
                <li>Business</li>
                <li>Banking & Finance</li>
                <li>Photography</li>
                <li>IT & Software</li>
                <li>Web Development</li>
              </ul>
              <div class="resp-tabs-container hor_1">
                <!-- Business -->
                <div>
                  <!-- card row -->
                  <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                           
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->

                          <!-- card hover block -->
                          <div class="cardHover">
                              <h6>Data Science Architect Master Course</h6>

                              <p class="fgray">in Collaboration with <span class="fbold">IBM</span></p>

                              <div class="d-flex justify-content-center py-3">
                                  <div class="px-3">
                                      <p class="text-center small">10</p>
                                      <p class="text-center small">Courses</p>
                                  </div>
                                  <div class="px-3 border-left border-right">
                                      <p class="text-center small">53</p>
                                      <p class="text-center small">Projects</p>
                                  </div>
                                  <div class="px-3">
                                      <p class="text-center small">232</p>
                                      <p class="text-center small">Hours</p>
                                  </div>                                  
                              </div>
                              <p>
                                <small>Key Skills - Data science with Python for Data Science, Machine Learning, Artificial Intelligence....</small>
                              </p>
                              <p class="text-center">
                                <a href="javascript:void(0)" class="pinkbtnlg mr-2">View Details</a>
                              </p>                              
                          </div>
                          <!-- /card hover block -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                          <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course04.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course05.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course08.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">Writing With Flair: How To Become An Exceptional Writer</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Courses</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Business -->


                <!-- Banking & Finance-->
                <div>
                  <!-- card row -->
                  <div class="row">

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course05.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->
                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Courses</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Banking & Finance -->

                <!-- Fixing stuff -->
                <div>
                  <!-- card row -->
                  <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course01.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->                  

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Courses</a>
                      </p>
                    </div>
                    <!--/ col 12 -->                  
                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Fixing stuff-->
                <!--Hosting a party -->
                <div>
                  <!-- card row -->
                  <div class="row">

                    <!-- col -->
                    <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course06.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course05.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                              <span class="small fgray">2K+ Learners</span>
                              <span class="small fgray">322 Hrs</span>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Courses</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Hosting a party -->
                <!-- Something different -->
                <div>
                  <!-- card row -->
                  <div class="row">

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course07.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                         <!-- card footer -->
                         <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course03.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                           <!-- card footer -->
                           <div class="card-footer d-flex justify-content-between">
                              <span class="small fgray">2K+ Learners</span>
                              <span class="small fgray">322 Hrs</span>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course04.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-sm-6">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <a href="javascript:void(0)"><img src="img/data/course02.jpg" alt="" class="img-fluid"></a>
                            <h4 class="h6"> 
                            <a href="javascript:void(0)">The Ultimate Hands-On Hadoop - Tame your Big Data!</a>
                            </h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <p><small class="ratespan"><span class="icon-star icomoon"></span> 4.5 </small><small>(25)</small></p>
                            <p><small>Sundog Education by Frank Kane</small></p>                
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="small fgray">2K+ Learners</span>
                            <span class="small fgray">322 Hrs</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                   

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Courses</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Something different-->
              </div>
            </div>
            <!-- tab -->
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ see others get  done -->

    <!-- how it works -->
    <div class="howitworks home-section">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-center pb-3">
          <!-- col -->
          <div class="col-lg-6">
            <article class="text-center">
              <h2 class="pb-2 text-center">Free Test's for Students</h2>
              <p>Choose from over 100,000 online video courses with new additions published every month</p>
            </article>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->      
        </div>
        <!--/ container -->

        <!-- container -fluid -->
        <div class="container-fluid">
           <!-- slider -->
          <div class="custom-slick freetests">
             <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              <!-- slide -->
              <div class="slide">
                <!-- card -->
                <div class="card test-item">
                  <!-- card body -->
                  <div class="card-body">
                      <p class="text-right">
                        <span class="bluebtnlg text-uppercase">free</span>
                      </p>
                      <h5 class="h6 fbold">
                        <a href="javascript:void(0)" class="fblue">IDBI Asst. Manager - Full Mock Test</a>
                      </h5>
                      <p class="fgray pb-4">IDBI Asst Manatger</p>

                      <p class="fgray">Expires on: June 30, 2020</p>
                      <p class="fblue small pb-3">Syllabus</p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Questions</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Marks</span>
                          <span class="fpink">200</span>
                      </p>
                      <p class="d-flex justify-content-between rowp">
                          <span class="fgray">Minutes</span>
                          <span class="fpink">120</span>
                      </p>
                  </div>
                  <!--/ card body -->
                  <!-- card footer -->
                  <div class="card-footer text-center">
                    <p>
                      <a href="javascript:void(0)" class="brdlink">Start Now</a>
                    </p>
                  </div>
                  <!--/ card footer -->
                </div>
                <!--/ card -->
              </div>
              <!--/ slide -->

              

          </div>
          <!--/ slider -->
        </div>
        <!-- /container -fluid -->
      </div>
    </div>
    <!--/ how it works -->

    <!-- list taskers -->
    <div class="graybox taskers-home py-3">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-center pb-5 pb-sm-2">
          <!-- col -->
          <div class="col-lg-10 text-center">
            <h2 class="">Meet some our Great Faculties and Trainers</h2>           
          </div>
          <!--/ col -->
          <!-- col 12 -->
          <div class="col-lg-12">
            <p class="text-center d-flex justify-content-center">
              <a href="javascript:void(0)" class="pinkbtnlg mt-0">Become a Trainer?</a>
            </p>
          </div>
          <!--/ col 12 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->

      <!-- container fluid -->
      <div class="container-fluid">
        <!-- slider -->
        <div class="custom-slick taskers">
          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker01.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Samantha</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker02.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Emily</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
               
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker03.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Samantha</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
                
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker04.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Brendan</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
                
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker05.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Praveen Guptha</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
                
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker06.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Prabhakar</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
                
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker07.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">John Peter</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
               
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker08.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Raja Gonda</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
                
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker09.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Suresh</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
                
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Hyderabad, Telangana</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker10.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Trainer Name will be here</a></h6>
                <p class="pb-2 fblue"><i class="small fgray">Specialist in System Admin</i></p>
               
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

        </div>
        <!--/ slider -->
      </div>
      <!--/ container fluid -->
    </div>
    <!--/ list taskers -->

    <!-- blog -->
    <div class="blog-section home-section graybox">
      <!--- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-lg-center">
          <!-- col -->
          <div class="col-lg-8 text-center">
            <h2 class="pb-2 ">Articles, stories and more</h2>
            <p>Whether you’re getting work done or doing tasks on Airtasker, know that we’ve got your back every step of
              the way.</p>
            <p class="text-center d-flex justify-content-center">
              <a href="javascript:void(0)" class="pinkbtnlg mr-2">View Our All Blog</a>
            </p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row pt-4">
          <!-- col -->
          <div class="col-sm-4">
            <div class="card">
              <img class="card-img-top" src="img/data/blog01.jpg" alt="Card image cap" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">30 Elegant art deco bathrooms</h5>
                <p class="card-text pb-4">Create a space that exudes elegance and gives a nod to the old world glamour
                  of
                  yesteryear. </p>
                <a href="#" class="bluebtnlg">Read More</a>
              </div>
            </div>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-sm-4">
            <div class="card">
              <img class="card-img-top" src="img/data/blog02.jpg" alt="Card image cap" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">40 Unbelievable U-shaped kitchen designs</h5>
                <p class="card-text pb-4">A practical solution that maximises bench space and storage! </p>
                <a href="#" class="bluebtnlg">Read More</a>
              </div>
            </div>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-sm-4">
            <div class="card">
              <img class="card-img-top" src="img/data/blog03.jpg" alt="Card image cap" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">The best bathroom vanity ideas for your home</h5>
                <p class="card-text pb-4">35 Functional and beautiful bathroom vanities that you will want in your home
                </p>
                <a href="#" class="bluebtnlg">Read More</a>
              </div>
            </div>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ blog -->

    <!-- clients -->
    <div class="homeClients">
        <!-- container -->
        <div class="container">
            <!-- row -->
           <div class="row">
              <!-- col -->
              <div class="col-lg-4">
                <div class="clientsleft">
                    <h6 class="h6">Heart From Top Corporates </h6>
                    <iframe width="100%" height="180" src="https://www.youtube.com/embed/_uQrJ0TkZlc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    
                    <p class="small text-center mb-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora minima odio nemo.</p>
                    <a href="javascript:void(0)" class="brdlink w-100 text-center">Contact us</a>
                </div>
            </div>
              <!--/ col -->
              <!-- col -->
              <div class="col-lg-8">
                    <h6 class="h6 pt-3 pt-sm-2">Our Clients</h6>
                    <!-- clients logos -->
                    <ul class="clientsLogos d-flex">
                      <li><img src="img/brand01.jpg"></li>
                      <li><img src="img/brand02.jpg"></li>
                      <li><img src="img/brand03.jpg"></li>
                      <li><img src="img/brand04.jpg"></li>
                      <li><img src="img/brand05.jpg"></li>
                      <li><img src="img/brand06.jpg"></li>
                      <li><img src="img/brand07.jpg"></li>
                      <li><img src="img/brand08.jpg"></li>
                      <li><img src="img/brand01.jpg"></li>
                      <li><img src="img/brand02.jpg"></li>
                      <li><img src="img/brand03.jpg"></li>
                      <li><img src="img/brand04.jpg"></li>
                      <li><img src="img/brand05.jpg"></li>
                      <li><img src="img/brand06.jpg"></li>
                      <li><img src="img/brand07.jpg"></li>
                      <li><img src="img/brand08.jpg"></li>
                      <li><img src="img/brand01.jpg"></li>
                      <li><img src="img/brand02.jpg"></li>
                      <li><img src="img/brand03.jpg"></li>
                      <li><img src="img/brand04.jpg"></li>
                    </ul>
                    <!-- /clients logos -->
              </div>
              <!--/ col -->
           </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ clients -->

  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
  

</body>

</html>