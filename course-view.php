<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body class="innerheader">
    <?php include 'header-course.php' ?>
  <!-- main -->
  <main class="subpage">

    <!-- container fluid -->
    <div class="container-fluid">
        <!-- row -->
        <div class="row pt-3">
            <!-- col -->
        <div class="col-lg-8">
            <iframe width="100%" height="560" src="https://www.youtube.com/embed/_uQrJ0TkZlc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
        </div>
        <!-- col -->
         <!-- col -->
         <div class="col-lg-4">
             <!-- course content -->
             <div class="course-content py-4">
                <!-- title -->
                <div class="d-flex justify-content-between">
                <h3 class="h5">Course Content</h3>
                <p class="text-right">04:58:43   <span class="seperator">|</span> 34 lectures</p>
                </div>
                <!--/ title -->

                <!-- courses list -->
                <!-- custom accord -->
                <div class="accordion cust-accord">

                    <h3 class="panel-title">Course Content</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">The Basics</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">De Bugging</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">Components and Data Binding</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">Course Content</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">The Basics</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">De Bugging</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                    <h3 class="panel-title">Components and Data Binding</h3>
                    <div class="panel-content">
                        <ul class="videos-list">
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Course Introduction</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> What is Angular?</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Join our Online Learning Community</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> Angular vs Angular 2 vs Angular 9</a>
                                <span>08:46</span>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><span class="icon-play-circle icomoon"></span> CLI Deep Dive & Troubleshooting</a>
                                <span>08:46</span>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /custom accord -->
                <!--/ courses list -->
            </div>
            <!--/ course content -->
         </div>
        <!-- col -->
        </div>
        <!--/ row -->
    </div>
    <!-- /container fluid -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
</body>

</html>