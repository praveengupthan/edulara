  <div class="mobileProfile justify-content-between" id="mobileProfile">
        <div><h6>User Name will be ...</h6></div>
       <div> <span class="icon-bars icomoon"></span></div>
  </div>
  
  <!-- user left nav -->
  <div class="user-leftnav sticky-top" id="userLeftNav">
        <!-- profile image-->
        <div class="profile-pic">
            <figure>
                <a href="account-settings-profile.php">
                    <img src="img/data/tasker02.jpg" class="md-thumb">
                    <span class="icon-edit icomoon"></span>
                </a>
            </figure>
            <a href="javascript:void(0)">Praveen Guptha</a>
        </div>
        <!--/ profile iagme -->  

        <!--nav -->
        <div class="profile-nav">
            <ul class="nav flex-column flex-nowrap overflow-hidden">
                <li class="nav-item">
                    <a class="nav-link" href="account-settings-profile.php"> Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-courses.php"> My Courses</a>
                </li>               
                <li class="nav-item">
                    <a class="nav-link" href="account-payment-methods.php"> Payment Methods</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-wishlistcourses.php"> My Wishlists</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-settings-changepassword.php"> Change Password</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-notifications.php"> Notifications</a>
                </li>  
                <li class="nav-item">
                    <a class="nav-link" href="index.php"> Logout</a>
                </li>                                                
            </ul>
        </div>
        <!--/ nav -->
    </div>
    <!--/ user left nav -->

   