<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account My Courses</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 col-sm-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9 col-sm-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">My Courses (25)</h1>

                     <!-- course item-->
                     <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="course-view.php"><img src="img/data/course01.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="course-view.php">Angular - The Complete Guide (2020 Edition)</a>
                                <span class="small fpink fbold">1% Complete</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)"><span class="icon-share-square-o"></span> Share</a> 
                                <a href="course-detail">View Syllabus</a>                                         
                            </p>
                        </article>
                    </div>
                    <!--/ course item -->

                     <!-- course item-->
                     <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="course-view.php"><img src="img/data/course02.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="course-view.php">The Complete JavaScript Course 2020: Build Real Projects!</a>
                                <span class="small fpink fbold">1% Complete</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)"><span class="icon-share-square-o"></span> Share</a> 
                                <a href="course-detail">View Syllabus</a>                                         
                            </p>
                        </article>
                    </div>
                    <!--/ course item -->

                     <!-- course item-->
                     <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="course-view.php"><img src="img/data/course03.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="course-view.php">Advanced CSS and Sass: Flexbox, Grid, Animations and More!</a>
                                <span class="small fpink fbold">1% Complete</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)"><span class="icon-share-square-o"></span> Share</a> 
                                <a href="course-detail">View Syllabus</a>                                         
                            </p>
                        </article>
                    </div>
                    <!--/ course item -->

                     <!-- course item-->
                     <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="course-view.php"><img src="img/data/course04.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="course-view.php">Material for Angular 6 - UI UX Ivy League Instructor</a>
                                <span class="small fpink fbold">1% Complete</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)"><span class="icon-share-square-o"></span> Share</a> 
                                <a href="course-detail">View Syllabus</a>                                         
                            </p>
                        </article>
                    </div>
                    <!--/ course item -->

                     <!-- course item-->
                     <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="course-view.php"><img src="img/data/course05.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="course-view.php">Simple And Easy: Wordpress For Beginners</a>
                                <span class="small fpink fbold">1% Complete</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)"><span class="icon-share-square-o"></span> Share</a> 
                                <a href="course-detail">View Syllabus</a>                                         
                            </p>
                        </article>
                    </div>
                    <!--/ course item -->

                     <!-- course item-->
                     <div class="graybox d-block d-sm-flex cart-item">
                        <figure>
                            <a href="course-view.php"><img src="img/data/course06.jpg" alt=""></a>
                        </figure>
                        <article class="align-self-center">
                            <h4 class="d-flex justify-content-between mb-1">
                                <a href="course-view.php">Angular - Mastering the Basics</a>
                                <span class="small fpink fbold">1% Complete</span>
                            </h4>
                            <p class="fgray small pb-4">Chip Reaves, Bigger Brains</p>

                            <p class="pb-0 links">
                                <a href="javascript:void(0)"><span class="icon-share-square-o"></span> Share</a> 
                                <a href="course-detail">View Syllabus</a>                                         
                            </p>
                        </article>
                    </div>
                    <!--/ course item -->

                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                 
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>