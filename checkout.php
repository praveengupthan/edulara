<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edulara</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header-postlogin.php' ?>
  <!-- main -->
  <main class="subpage">
      <!-- apge header -->
      <div class="page-header">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-8">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a href="courses.php">Courses</a></li>
                            <li class="breadcrumb-item"><a href="cart.php">Cart</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                        </ol>
                    </nav>
                    <h1>Checkout</h1>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
      </div>
      <!--/ page header -->

      <!-- pge body -->
      <div class="page-body">

      <!-- container -->
      <div class="container">
            <!-- row -->
            <div class="row">
                <!-- left col -->
                <div class="col-lg-8 col-sm-8">
                    <h2 class="h4">Checkout</h2>
                    <p class="py-2">
                        <input type="radio"> VISA ending in 7997 <span><img src="img/card-visa.png"></span>
                    </p>
                    <p class="py-2">
                        <input type="radio"> Pay with Net banking 
                    </p>
                    <p class="py-2">
                        <input type="radio"> Pay with Paypal <span><img src="img/paypal.png"></span>
                    </p>
                    <p class="py-2">
                        <input type="radio"> Pay by mobile using other methods 
                    </p>

                    <a href="javascript:void(0)" class="fbold fblue d-inline-block pb-2">Add New Card</a>
                    <p>By completing your purchase you agree to these <a href="javascript:void(0)" class="fblue">Terms of Service.</a></p>
                </div>                
                <!--/ left col -->
                 <!-- right col -->
                 <div class="col-lg-4 col-sm-4">
                    <h2 class="h4 pb-3">Summary</h2>

                    <p class="d-flex justify-content-between py-2">
                        <span>Original price:</span>
                        <span>Rs:2500.00</span>
                    </p>
                    <p class="d-flex justify-content-between py-2 border-bottom">
                        <span>Coupon Discount:</span>
                        <span>-Rs:2500.00</span>
                    </p>
                    <h5 class="d-flex justify-content-between py-4">
                        <span>Total:</span>
                        <span class="fblue">Rs:2500.00</span>
                    </h5>
                    <p class="pb-3 mb-3">
                        <a href="javascript:void(0)" class="pinkbtnlg">Complete Payment</a>
                     </p>


                </div>                
                <!--/ right col -->
            </div>
            <!--/ row -->
      </div>
      <!--/ container -->
           
      </div>
      <!--/ page bodyt -->
  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
</body>

</html>