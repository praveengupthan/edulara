<!-- footer -->
  <footer>
    <!-- container -->
    <div class="container">
        <div class="w-75 mx-auto border-bottom py-3 mb-4">
           <!-- row -->
          <div class="row justify-content-center">
              <!-- col -->
              <div class="col-lg-4 align-self-center">
                  <h6 class="h6 fwhite">We Are featured on:</h6>
              </div>
              <!--/ col -->
              <!-- col -->
              <div class="col-lg-8">
                <!-- clients logos -->
                <ul class="clientsLogos d-flex">
                    <li><img src="img/brand01.jpg"></li>
                    <li><img src="img/brand02.jpg"></li>
                    <li><img src="img/brand03.jpg"></li>
                    <li><img src="img/brand04.jpg"></li>  
                    <li><img src="img/brand05.jpg"></li>                 
                  </ul>
                  <!-- /clients logos -->
              </div>
              <!--/ col -->
          </div>
          <!--/ row -->
        </div>
      <!-- row -->
      <div class="row">
        <!-- col -->
        <div class="col-lg-3 col-sm-6">
          <h5 class="h5">Discover</h5>
          <ul>
            <li><a href="javascript:void(0)">How it works</a></li>
            <li><a href="javascript:void(0)">Laratasker for business</a></li>
            <li><a href="javascript:void(0)">Earn money</a></li>
            <li><a href="javascript:void(0)">New users FAQ</a></li>
            <li><a href="javascript:void(0)">Find work</a></li>
          </ul>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-3 col-sm-6">
          <h5 class="h5">Company</h5>
          <ul>
            <li><a href="javascript:void(0)">About us</a></li>
            <li><a href="javascript:void(0)">Careers</a></li>
            <li><a href="javascript:void(0)">Community guidelines</a></li>
            <li><a href="javascript:void(0)">Terms & conditions</a></li>
            <li><a href="javascript:void(0)">Blog</a></li>
            <li><a href="javascript:void(0)">Contact us</a></li>
            <li><a href="javascript:void(0)">Privacy Policy</a></li>
          </ul>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-3 col-sm-6">
          <h5 class="h5">Existing Members</h5>
          <ul>
            <li><a href="javascript:void(0)">Your Courses</a></li>
            <li><a href="javascript:void(0)">Browse Course</a></li>
            <li><a href="javascript:void(0)">Login</a></li>
            <li><a href="javascript:void(0)">Support centre</a></li>
            <li><a href="javascript:void(0)">Merchandise</a></li>
          </ul>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-3 col-sm-6">
          <h5 class="h5">Popular Categories</h5>
          <ul>
            <li><a href="javascript:void(0)">Computer &amp; Software</a></li>
            <li><a href="javascript:void(0)">Photography</a></li>
            <li><a href="javascript:void(0)">Delivery Services</a></li>
            <li><a href="javascript:void(0)">Removalists</a></li>
            <li><a href="javascript:void(0)">Gardening Services</a></li>
            <li><a href="javascript:void(0)">Automotive</a></li>
            <li><a href="javascript:void(0)">Assembly Services</a></li>
            <li><a href="javascript:void(0)">All Services</a></li>
          </ul>
        </div>
        <!--/ col -->
      </div>
      <!--/ row -->
    </div>
    <!--/ container -->

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row justify-content-center">
            <!-- col =-->
            <div class="col-lg-8">
                <div class="contact-footer">
                    <div>
                      <h5 class="h4 fgray">Need more info?</h5>
                      <p class="small">IND: +91 9642123254 US:1-800-216-8930 (Toll Free) |  sales@edulara.com</p>
                    </div>
                    <div class="align-self-center">
                        <a href="javascript:void(0)" class="pinkbtn d-inline-block px-3">Request a Call</a>
                    </div>
                </div>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->

    <!-- footer bottom -->
    <div class="bottom-footer">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <!-- col -->
          <div class="col-lg-6 socialcol">
            <a href="javascript:void(0)">
              <span class="icon-facebook icomoon"></span>
            </a>
            <a href="javascript:void(0)">
              <span class="icon-twitter icomoon"></span>
            </a>
            <a href="javascript:void(0)">
              <span class="icon-linkedin icomoon"></span>
            </a>
            <a href="javascript:void(0)">
              <span class="icon-instagram icomoon"></span>
            </a>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-6 rightcol align-self-center">
            <p><span class="icon-logo icomoon"></span> Pty. Ltd 2011-2020©, All rights reserved</p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ footer bottom -->

    <a href="javascript:void(0)" id="movetop" class="movetop">
      <span class="icon-arrow-up icomoon"></span>
    </a>


  </footer>
  <!--/ footer -->

  